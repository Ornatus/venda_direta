@extends('layouts.app')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script type="text/javascript" src="{{ URL::asset('js/jquery-2.1.1.min.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.7.0/moment.min.js" type="text/javascript"></script>
				<script type="text/javascript" src="{{ URL::asset('selectpicker/dist/js/bootstrap-select.min.js') }}"></script>
								<script type="text/javascript" src="{{ URL::asset('selectpicker/dist/js/i18n/defaults-pt_BR.min.js') }}"></script>
								<link rel="stylesheet" type="text/css" href="{{ URL::asset('tablesorter/themes/blue/style.css') }}">

<style>
.navbar_bottom {
  overflow: hidden;
  background-color:  #FFFFFF;
  position: fixed;
  bottom: 0;
  border:1px solid #d3e0e9;
  width: 100%;
  padding:10px !important;
}

.navbar_bottom a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.navbar_bottom a:hover {
  background: #ddd;
  color: black;
}
</style>
@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-18">
      <div class="panel panel-default">
        <div class="panel-body">
           <div id="content">
             <div class="box">
                <div class="heading">
                  <h3><i class="fa fa-gift" aria-hidden="true"></i> Configuração de kit </h3>
                </div>
                <div class="content" style="overflow:visible !important">
                <!--<input type="button" class="btn btn-primary add-kit pull-right add_kitnews" name="add_kitnews" id="add_kitnews" value="Novo kit"> -->
               <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
               <input type="button" class="btn btn-info list-next-kit" name="next-kits" id="next-kits" value=" Próximos kits">
                <input type="button" class="btn btn-default list-history-kit" name="history-kits" id="history-kits" value=" Histórico">
                <div class="pagination"></div>
                <div id="grid-kit">
                     <?php echo $html;?>
                </div>
              </div>
              </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
</div>

<script>
$(document).ready(function() {

    $('.add_kitnews').click(function() {
          var url = "{{ $url_new_kit }}";

        $.ajax({
            url: url,
            type: 'GET',
            success: function(data) {


                var data_return = JSON.parse(data);

                var date_start = moment(data_return.date_start, 'YYYY-MM-DD').format('DD/MM/YYYY');
                var date_end = moment(data_return.date_end, 'YYYY-MM-DD').format('DD/MM/YYYY');

                Swal.fire({
                  title: 'Confirmar?',
                  text: "Criar kit de " + date_start + " até " + date_end,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Sim',
                  cancelButtonText: "Não"
                }).then((result) => {
                  if (result.value) {
                        saveKit(data_return.date_start, data_return.date_end);
                  }
                });

            },
            cache: false,
            contentType: false
        });
     });

     function saveKit(date_start, date_end) {
           var url = "{{ $url_create_kit }}";

           $.ajax({
               url: url,
               type: 'POST',
               data: {"date_start": date_start, "date_end": date_end, "_token":"{{ csrf_token() }}"},
               datatype : "application/json",
               success: function(dataReturn) {
                   if (dataReturn > 0) {
                       Swal.fire("Cofirmado ", "Kit criado com sucesso.", "success");
                        reloadGridKit();
                       return true;
                   } else {
                       Swal.fire("Erro ", "Não foi possivel salvar o kit.", "error");
                       return false;
                   }
               },
               error: function(xhr, status, error) {
                   Swal.fire("Erro ", "Não foi possivel Salvar o kit(2).", "error");
                   return false;
               }
           });

     }


     function reloadGridKit() {
         var url = "{{ $url_grid_kit }}";

         //loading
         $("#grid-kit").html("<div class='text-center'><i class='fa fa-spinner fa-spin pull-center' style='font-size:20px'></i></div>");

         $.ajax({
             url: url,
             type: 'GET',
             success: function(data) {
                 $("#grid-kit").html(data.html);
             }
         });

     	$("#next-kits").removeClass('btn-default');
     	$("#next-kits").addClass('btn-info');

     	$("#history-kits").removeClass('btn-info');
     	$("#history-kits").addClass('btn-default');

     	 return true;

     }

     function getGridHistoryKit() {
        var url = "{{ $url_history_kit }}";
         //loading
         $("#grid-kit").html("<div class='text-center'><i class='fa fa-spinner fa-spin pull-center' style='font-size:20px'></i></div>");

         $.ajax({
             url: url,
             type: 'GET',
             success: function(data) {
                 $("#grid-kit").html(data.html);

             }
         });

     	$("#next-kits").removeClass('btn-info');
     	$("#next-kits").addClass('btn-default');

     	$("#history-kits").removeClass('btn-default');
     	$("#history-kits").addClass('btn-info');

     	return true;
     }


     $('.list-next-kit').click(function() {

     	reloadGridKit();

     });

     $('.list-history-kit').click(function() {

     	getGridHistoryKit();

     });





});

</script>
@endsection
