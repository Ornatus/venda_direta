@foreach($kitDatas as $kit)
  <div class="panel-group">
    <div class="panel ">
      <div class="panel-heading">
        <table width="100%">
          <tr>
            <td class="left" style="width:8%">Kit  <br />
              <b> <span class="badge"> <?php echo $kit->name; ?> </b></span>
            </td>
            <td class="left" style="width:20%">Data inicio  <br />
              <b><?php echo date("d/m/Y",strtotime($kit->date_start)); ?>
            </td>
            <td class="left" style="width:20%">Data Fim : <br />
              <b><?php echo date("d/m/Y",strtotime($kit->date_end)); ?>
            </td>
            <td class="left" style="width:20%"> Nível  <br />
              <b><?php echo $kit->level; ?>
            </td>
            <td class="left" style="width:20%"> Status  <br />
              <b><?php echo $kit->confirmed == 'N' ? '<span class="badge" style="background:red !important"> Não confirmado </span>' : '<span class="badge" style="background:green !important"> confirmado </span>'; ?>
            </td>
              <td style="text-align:right;width:10%">
                <a data-toggle="collapse" href="#collapse{{ $kit->id }}">
                  <a href="{{ url('kit/kitConfigById/'.$kit->id ) }}"  data-toggle="tooltip"  class="btn btn-success"><i class="fa fa-cog fa-fw"></i>Detalhes</a>
                </a>
              </td>
            </tr>
          </table>
        </div>


    </div>
  </div>
@endforeach

<style>

/* visited link */
a:visited {
  color: white;
}
</style>
