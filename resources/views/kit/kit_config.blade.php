@extends('layouts.app')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script type="text/javascript" src="{{ URL::asset('js/jquery-2.1.1.min.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.7.0/moment.min.js" type="text/javascript"></script>
				<script type="text/javascript" src="{{ URL::asset('selectpicker/dist/js/bootstrap-select.min.js') }}"></script>
								<script type="text/javascript" src="{{ URL::asset('selectpicker/dist/js/i18n/defaults-pt_BR.min.js') }}"></script>
								<link rel="stylesheet" type="text/css" href="{{ URL::asset('tablesorter/themes/blue/style.css') }}">
                <script type="text/javascript" src="{{ URL::asset('js/jquery.maskMoney.js') }}"></script>

<style>
.navbar_bottom {
  overflow: hidden;
  background-color:  #FFFFFF;
  position: fixed;
  bottom: 0;
  border:1px solid #d3e0e9;
  width: 100%;
  padding:10px !important;
}

.navbar_bottom a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.navbar_bottom a:hover {
  background: #ddd;
  color: black;
}
</style>
@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-18">
      <div class="panel panel-default">
        <div class="panel-body">
           <div id="content">
             <div class="box">
                <div class="heading">
                  <div class="col-md-12">
                    <a style="float:right !important" href="{{ URL::to('/kit/kitList') }}" class="btn button" ><i class="fas fa-reply"></i> Voltar</a>

                  <h3><i class="fa fa-gift" aria-hidden="true"></i> Configuração do kit " <?php echo $kit_id; ?> "</h3></div>
                  <div class="col-md-6">
                                      <br />
                                      <br />
                                      <?php
                                        $disabled ='';
                                      if($kitData['confirmed'] =='S')
                                      {
                                        $disabled ='disabled';
                                      }
                                      ?>
                                    </div>
                  <div class="col-md-12">
                  <form class="form-horizontal" method="POST" action="{{ action('Kit\KitController@kitUpdate')  }}">
                      {{ csrf_field() }}

                      <div class="form-group col-md-offset-2" >
                          <label for="name" class="col-md-2 control-label">Nome</label>

                          <div class="col-md-3 ">
                              <input id="name" type="text" class="form-control" name="name" value="<?php echo $kitData['name'] ?>" required autofocus <?php echo $disabled?>>
                          </div>
                      </div>
                      <div class="form-group col-md-offset-2" >
                          <label for="date_start" class="col-md-2 control-label">Data inicio</label>

                          <div class="col-md-3 ">
                              <input id="date_start" type="date" class="form-control" name="date_start" value="<?php echo date("Y-m-d",strtotime($kitData['date_start'])); ?>" required autofocus <?php echo $disabled?>>
                          </div>
                      </div>
                      <div class="form-group col-md-offset-2" >
                          <label for="date_end" class="col-md-2 control-label">Data fim</label>

                          <div class="col-md-3 ">
                              <input id="date_end" type="date" class="form-control" name="date_end" value="<?php echo date("Y-m-d",strtotime($kitData['date_end'])); ?>" required autofocus <?php echo $disabled?>>
                          </div>
                      </div>
                      <input id="kit_id" type="hidden" class="form-control" name="kit_id" value="<?php echo $kit_id; ?>" >
                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-2">
                            <?php if($kitData['confirmed'] =='N') { ?>
                              <button type="submit" class="btn btn-primary" >
                                  Salvar
                              </button>
                              <input type="button" class="btn btn-danger kitConfirm" data-kit-id="<?php echo $kit_id; ?>" value="Finalizar" />
                           <?php } ?>

                          </div>
                      </div>
                  </form>
                </div>
                  <div class="content" style="overflow:visible !important">
                    <?php if($kitData['confirmed'] =='N') { ?>
                    <!-- esse chama modal com todos os produtos cadastrados <div style="height:30px; margin:10px">
                       <input
                          type="button"
                          class="btn pull-right btn-primary btn-modal-product"
                          id="edit-kit-<?php echo $kit_id; ?>"
                          data-toggle="modal"
                          data-target="#myModal"
                          data-kit="<?php echo $kit_id; ?>"
                     value="Add Produtos"> -->
                       <input id="new_product" type="button" class="btn pull-right btn-primary" data-toggle="modal" data-target=".ModalPreview" data-kit-product="<?php echo $kit_id; ?>"  value="+ Produto" />
                   <?php } ?>
                   </div>

                   <div id="grid-kit" class="clear">
                        <?php echo $html;?>
                   </div>

                   <div class="panel-body" style="border-top-color:white !important"><div id="<?php echo $kit_id; ?>">
                       </div>
                     </div>
                   <div class="panel-footer"></div>
                 </div>
              </div>
              </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
</div>
<!-- Modal -->
<div class="modal fade modal-products" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="width:80% !important" role="document">
    <div class="modal-content" style="width: 100% !important">
      <div class="modal-header" style="height:40px !important">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="window.location.reload();">×</button>
        <input type="hidden" id="kit_id" value=""/>
        <h5 class="modal-title" id="modal-title"> </h5>
        </button>
      </div>
      <div id="modal-body" class="modal-body" style="overflow-y: auto !important;max-height: 500px !important;">
      </div>
    </div>
  </div>
</div>
<!-- aqui modal para inserir produto-->
<div class="modal fade ModalPreview" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

  <div class="modal-dialog modal-lg" style="width:80% !important">
    <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar Produto <span id="span-kit"></span> </h4>
       </div>
         <div class="modal-body" style="height:415px !important">
           <form class="form-horizontal">
           <div class="form-group">
             <label class="col-sm-3 control-label" for="input-email">Codigo Brasil:</label>
             <div class="col-sm-8">
               <input type="number" id="brazil_code" value="" placeholder="Codigo Brasil"  class="form-control" required autofocus/>
             </div>
           </div>
           <div class="form-group">
             <label class="col-sm-3 control-label" for="input12">Codigo Brasil Novo:</label>
             <div class="col-sm-8">
               <input type="number" id="new_brazil_code"  value="" placeholder="Codigo Brasil Novo"  class="form-control" required autofocus/>
             </div>
           </div>
           <div class="form-group">
             <label class="col-sm-3 control-label" for="input12">Quantidade:</label>
             <div class="col-sm-8">
               <input type="number" id="quantity"  value="" placeholder="Quantidade"  class="form-control" required autofocus/>
             </div>
           </div>
           <div class="form-group">
             <label class="col-sm-3 control-label" for="input12">Preço:</label>
             <div class="col-sm-8">
               <input type="text"  id="price" onKeyPress="return(moeda(this,'.',',',event))" value="" placeholder="Preço"  class="form-control" required autofocus/>
             </div>
           </div>
           <div class="form-group">
             <label class="col-sm-3 control-label" for="input12">Preço Revendedora:</label>
             <div class="col-sm-8">
               <input type="text"  id="representative_price" onKeyPress="return(moeda(this,'.',',',event))"  value="" placeholder="Preço Revendedora"  class="form-control" required autofocus/>
             </div>
           </div>
           <div class="form-group">
             <label class="col-sm-3 control-label" for="input12">Descrição:</label>
             <div class="col-sm-8">
               <input type="text" id="description"  value="" placeholder="Descrição"  class="form-control" required autofocus/>
             </div>
           </div>
           <div class="form-group">
             <label class="col-sm-3 control-label" for="input12">Categoria:</label>
             <div class="col-sm-8">
               <input type="text" id="category"  value="" placeholder="Categoria"  class="form-control" required autofocus/>
             </div>
           </div>
           <div class="form-group">
             <label class="col-sm-3 control-label" for="input12">Cor:</label>
             <div class="col-sm-8">
               <input type="text" id="color"  value="" placeholder="Cor"  class="form-control" required autofocus/>
             </div>
             <input type="hidden" id="kit_product_id" value=""/>
           </div>
         </form>
         </div>
         <div class="modal-footer">
           <button type="button" id="close-modal" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
           <button type="button" id="save_product" class="btn btn-success">Adicionar</button>
         </div>
     </div>

  </div>
</div>
<script>
$(document).ready(function() {
  $(function() {
      clickButtonModalProduct();
      clickButtonShowProduct();
  });


    //modal
     function clickButtonModalProduct() {
         $('.btn-modal-product').click(function() {
             $("#modal-body").html('');
             $("#kit_id").val('');
             $("#kit_id").val($(this).data('kit'));
             $("#modal-title").html("Montagem do kit  " + $(this).data('kit'));
             getProducts($(this).data('kit'));
         });
     }

     function clickButtonShowProduct() {
         $('.btn-modal-product-show').click(function() {
         	getKitProducts($(this).data('kit'));
         });
     }

     function getProducts(kit_id, filter_brazil_code, filter_new_brazil_code, filter_category_id, page) {
        var url = "{{ $url_products }}";

         //loading
         $("#modal-body").html("<div class='text-center'><i class='fa fa-spinner fa-spin pull-center' style='font-size:20px'></i></div>");

         $.ajax({
           url: url,
           type: 'GET',
           data: {"kit_id": kit_id, "filter_brazil_code": filter_brazil_code, "filter_new_brazil_code": filter_new_brazil_code, "filter_category_id": filter_category_id, "page": page, "_token":"{{ csrf_token() }}" },
           datatype : "application/json",
           success: function(data) {

               $("#modal-body").html(data.html);
           },
           cache: false,
         });


     }

     function getKitProducts(kit_id, filter_brazil_code, filter_new_brazil_code) {
         var url = "{{ $url_kit_products }}";

         $.ajax({
           url: url,
           type: 'GET',
           data: {"kit_id": kit_id, "filter_brazil_code": filter_brazil_code, "filter_new_brazil_code": filter_new_brazil_code, "_token":"{{ csrf_token() }}" },
           datatype : "application/json",
           success: function(data) {
             $("#"+kit_id).html('');
                $("#"+kit_id).html(data.html);

           },
           cache: false,
         });

     }
     function reloadGridKit() {
         var url = "{{ $url_grid_kit }}";

         //loading
         $("#grid-kit").html("<div class='text-center'><i class='fa fa-spinner fa-spin pull-center' style='font-size:20px'></i></div>");

         $.ajax({
             url: url,
             type: 'GET',
             success: function(data) {
                 $("#grid-kit").html(data.html);
                 clickButtonModalProduct();
                 clickButtonShowProduct();
             }
         });

     	$("#next-kits").removeClass('btn-default');
     	$("#next-kits").addClass('btn-info');

     	$("#history-kits").removeClass('btn-info');
     	$("#history-kits").addClass('btn-default');

     	 return true;

     }
     // aqui precisa fazer
     $('.kitConfirm').click(function() {
          // var url = "{{ $url_new_kit }}";
             var kit_id = $(this).data('kit-id');

             swal.fire({
                     title: "Tem certeza?",
                     text: "Tem certeza que deseja confirmar este kit?",
                     type: "warning",
                     showCancelButton: true,
                     confirmButtonColor: "green",
                     confirmButtonText: "Sim",
                     cancelButtonText: "Não",
                     allowOutsideClick: false,
                     closeOnClickOutside: false

                   }).then((result) => {
                     if (result.value) {

                       swal.fire({
                  			title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
                  			text: "Aguarde..",
                  			showCancelButton: false,
                  			showConfirmButton: false,
                  			closeOnConfirm: false,
                  			showLoaderOnConfirm: false,
                  		});

                         var url = "{{ $url_confirm_kit }}";

                          $.ajax({
                              url: url,
                              type: 'POST',
                              data: {"kit_id": kit_id, "_token":"{{ csrf_token() }}"},
                              datatype : "application/json",
                              success: function(data) {
                                if (data > 0)
                                {
                                  location.reload();

                                } else
                                {
                                  swal.fire({
                                      title: "Erro",
                                      text: "Erro ao confirmar o kit.",
                                      type: "warning",
                                    });
                                  return false;
                                }

                              },
                              error: function(xhr, status, error) {

                              }
                          }).done(function() {



                          });
                     } else if (result.dismiss === Swal.DismissReason.cancel) {
                       swal.close();
                     }
                   })

      });



});


//add product
$(document).ready(function() {
  $('#new_product').click(function() {
      $("#span-kit").text(' No Kit ' + $(this).data('kit-product'));
      $("#kit_product_id").val($(this).data('kit-product'));
      $("#brazil_code").val('');
      $("#new_brazil_code").val('');
      $("#quantity").val('');
      $("#price").val('');
      $("#representative_price").val('');
      $("#description").val('');
      $("#category").val('');
      $("#color").val('');

  });
$('#save_product').click(function() {
          var kit_id = $("#kit_product_id").val();
          var brazil_code = $("#brazil_code").val();
          var new_brazil_code = $("#new_brazil_code").val();
          var quantity = $("#quantity").val();
          var price = $("#price").val();
          var representative_price = $("#representative_price").val();
          var description = $("#description").val();
          var category = $("#category").val();
          var color = $("#color").val();

           var url = "{{ $url_create_product }}";

           if(brazil_code == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe o Codigo Brasil !",
                 type: "warning",
               });
             return false;
           }
           if(new_brazil_code == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe o Codigo Brasil Novo !",
                 type: "warning",
               });
             return false;
           }
           if(quantity == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe a Quantidade !",
                 type: "warning",
               });
             return false;
           }
           if(price == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe o Preço !",
                 type: "warning",
               });
             return false;
           }
           if(representative_price == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe o Preço Revendedora !",
                 type: "warning",
               });
             return false;
           }
           if(description == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe a Descrição !",
                 type: "warning",
               });
             return false;
           }
           if(category == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe a Categoria !",
                 type: "warning",
               });
             return false;
           }
           if(color == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe a Cor !",
                 type: "warning",
               });
             return false;
           }

           swal.fire({
            title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
            text: "Aguarde..",
            showCancelButton: false,
            showConfirmButton: false,
            closeOnConfirm: false,
            showLoaderOnConfirm: false,
          });

              $.ajax({
                  url: url,
                  type: 'POST',
                  data: {"kit_id": kit_id,
                         "brazil_code": brazil_code,
                         "new_brazil_code": new_brazil_code,
                         "quantity": quantity,
                         "price": price,
                         "representative_price": representative_price,
                         "description": description,
                         "category": category,
                         "color": color,
                         "_token":"{{ csrf_token() }}"},
                  datatype : "application/json",
                  success: function(data) {
                    if (data > 0)
                    {
                    $("#kit_product_id").val(kit_id);
                    $("#brazil_code").val('');
                    $("#new_brazil_code").val('');
                    $("#quantity").val('');
                    $("#price").val('');
                    $("#representative_price").val('');
                    $("#description").val('');
                    $("#category").val('');
                    $("#color").val('');


                    $("#close-modal").click();
                     swal.close();
                     window.location.reload();
                    } else
                    {
                      swal.fire({
                          title: "Erro",
                          text: "Erro ao cadastrar o produto.",
                          type: "warning",
                        });
                      return false;
                    }

                  },
                  error: function(xhr, status, error) {

                  }
              }).done(function() {



              });


          });


      });

</script>
<script language="javascript">
function moeda(a, e, r, t) {
    let n = ""
      , h = j = 0
      , u = tamanho2 = 0
      , l = ajd2 = ""
      , o = window.Event ? t.which : t.keyCode;
    if (13 == o || 8 == o)
        return !0;
    if (n = String.fromCharCode(o),
    -1 == "0123456789".indexOf(n))
        return !1;
    for (u = a.value.length,
    h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
        ;
    for (l = ""; h < u; h++)
        -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
    if (l += n,
    0 == (u = l.length) && (a.value = ""),
    1 == u && (a.value = "0" + r + "0" + l),
    2 == u && (a.value = "0" + r + l),
    u > 2) {
        for (ajd2 = "",
        j = 0,
        h = u - 3; h >= 0; h--)
            3 == j && (ajd2 += e,
            j = 0),
            ajd2 += l.charAt(h),
            j++;
        for (a.value = "",
        tamanho2 = ajd2.length,
        h = tamanho2 - 1; h >= 0; h--)
            a.value += ajd2.charAt(h);
        a.value += r + l.substr(u - 2, u)
    }
    return !1
}
 </script>
@endsection
