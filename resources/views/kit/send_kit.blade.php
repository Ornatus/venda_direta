@extends('layouts.app')

<style>
.navbar_bottom {
  overflow: hidden;
  background-color:  #FFFFFF;
  position: fixed;
  bottom: 0;
  border:1px solid #d3e0e9;
  width: 100%;
  padding:10px !important;
}

.navbar_bottom a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.navbar_bottom a:hover {
  background: #ddd;
  color: black;
}
</style>
@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <form id="shopping_list" name="shopping_list" method="GET">
          <div class="panel-body">
            <h4><i class="fa fa-bars"></i> <b>Gerenciar Kits</b></h4>
            <div class="col-md-12">


              <table class="table" style="font-size:12pt !important">
                <thead>
                  <tr>
                    <th scope="col">Kit</th>
                    <th scope="col">Nível</th>
                    <th scope="col">Início</th>
                    <th scope="col">Fim</th>
                    <th scope="col" style="text-align:left; width:25%">Ações</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($kits as $kit){ ?>
                    <tr>
                      <td><?php echo $kit->name ?></td>
                      <td><?php echo $kit->level ?></td>
                      <td><?php echo date("d/m/Y",strtotime($kit->date_start)); ?></td>
                      <td><?php echo date("d/m/Y",strtotime($kit->date_end)); ?></td>
                      <td style="text-align:left">
                        <a type="button"  href="{{ URL::to('/representative/send-kit/representative-list/') }}/<?php echo $kit->id ?>" class="btn btn-default">Enviar produtos</a>
                        <a type="button"  href="{{ URL::to('/representative/settle-kit/representative-list/') }}/<?php echo $kit->id ?>" class="btn btn-default">Acerto</a>
                      </td>

                    </tr>
                  <?php } ?>
                </tbody>

              </table>

            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>



@endsection
