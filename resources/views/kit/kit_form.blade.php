@extends('layouts.app')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
				<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-18 ">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastro de Kit</div>

              <div class="panel-body">
                  
                  <br />
                    <form class="form-horizontal" method="POST" action="{{ action('Kit\KitController@kitCreateAtomatic')  }}">
                        {{ csrf_field() }}

                        <div class="form-group col-md-offset-2" >
                            <label for="name" class="col-md-2 control-label">Nome</label>

                            <div class="col-md-3 ">
                                <input id="name" type="text" class="form-control" name="name" value="" required autofocus>
                            </div>
                        </div>
                        <div class="form-group col-md-offset-2" >
                            <label for="date_start" class="col-md-2 control-label">Data inicio</label>

                            <div class="col-md-3 ">
                                <input id="date_start" type="date" class="form-control" name="date_start" value="" required autofocus>
                            </div>
                        </div>
                        <div class="form-group col-md-offset-2" >
                            <label for="date_end" class="col-md-2 control-label">Data fim</label>

                            <div class="col-md-3 ">
                                <input id="date_end" type="date" class="form-control" name="date_end" value="" required autofocus>
                            </div>
                        </div>
                        <input id="hash" type="hidden" class="form-control" name="hash" value="<?php echo $hash ; ?>" >
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <button type="submit" class="btn btn-primary" >
                                    Salvar
                                </button>

                            </div>
                        </div>
                    </form>
                    <div class="col-md-12">
                      <h5>Produtos</h5>
                      <table class="table" style="font-size:8pt !important">
                        <thead>
                          <tr>
                            <th class="col">Imagem</th>
                             <th class="col">Codigo Brasil</th>
                             <th class="col">Codigo Brasil Novo</th>
                             <th class="col">Preço</th>
                             <th class="col">Preço Revendedora</th>
                             <th class="col">Ganho</th>
                             <th class="col">Descrição</th>
                             <th class="col">Categoria</th>
                             <th class="col">Cor</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          foreach($products as $product)
                          {

                            ?>
                            <tr>
                              <td class="center table-grid-data">
                                    <img class="img" src="data:image/png;base64,<?php echo $product->image; ?>" alt="<?php echo $product->brazil_code; ?>" height="42" width="42" style="padding: 1px; border: 1px solid #DDDDDD;" />
                              </td>
                              <td class="left"><?php echo $product->brazil_code ; ?></td>
                              <td class="left"><?php echo $product->new_brazil_code ; ?></td>
                              <td class="left"><?php echo number_format($product->price, 2, '.', '') ; ?></td>
                              <td class="left"><?php echo number_format($product->representative_price, 2, '.', ''); ?></td>
                              <td class="left"><?php echo number_format(($product->representative_price - $product->price), 2, '.', ''); ?></td>
                              <td class="left"><?php echo $product->description ; ?></td>
                              <td class="left"><?php echo $product->category ;?></td>
                              <td class="left"><?php echo $product->color ;?></td>
                            </tr>
                          <?php
                          }
                          ?>


                        </tbody>

                      </table>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {

  $("form").submit(function (e) {

		 swal.fire({
			title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
			text: "Gerando os kits",
			showCancelButton: false,
			showConfirmButton: false,
			closeOnConfirm: false,
			showLoaderOnConfirm: false,
		});
 });

	});

</script>
@endsection
