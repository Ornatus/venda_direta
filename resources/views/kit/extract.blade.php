@extends('layouts.app')

<style>
.navbar_bottom {
  overflow: hidden;
  background-color:  #FFFFFF;
  position: fixed;
  bottom: 0;
  border:1px solid #d3e0e9;
  width: 100%;
  padding:10px !important;
}

.navbar_bottom a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.navbar_bottom a:hover {
  background: #ddd;
  color: black;
}
</style>
@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<div class="container" style="font-family:arial !important">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <form id="shopping_list" name="shopping_list" method="GET">
          <div class="panel-body">
            <h4><i class="fa fa-bars"></i> <b>Extrato</b></h4>
            <div class="col-md-12">

              <table width="100%" style="color:#006666">
                <tr>
                  <td>
                    <i class="fa fa-poll-h"></i> <?php echo $kit[0]->name; ?>
                  </td>
                  <td>
                    <i class="fa fa-user"></i> <?php echo $representative['name']; ?>
                  </td>
                  <td>
                    <i class="fa fa-phone-square"></i> <?php echo $representative['cellphone']; ?>
                  </td>
                </tr>
              </table>
              <br>
              <h4>Resumo</h4>
              <hr>
              <table class="table" style="font-size:10pt">
                <thead>
                  <tr>
                    <th scope="col">Vendas</th>
                    <th scope="col">Produtos Vendidos</th>
                    <th scope="col">Total Vendido</th>
                    <th scope="col">Total Ganho</th>
                    <th scope="col">Produtos a devolver</th>
                  </tr>
                  <tr style="font-size:12pt">
                    <th scope="col"><?php echo $total[0]->vendas; ?></th>
                    <th scope="col"><?php echo $total[0]->produtos_vendidos; ?></th>
                    <th scope="col"><?php echo $total[0]->total_price; ?></th>
                    <th scope="col"><?php echo $total[0]->total_ganho; ?></th>
                    <th scope="col"><?php echo ($total[0]->total_produtos)-($total[0]->produtos_vendidos); ?></th>
                  </tr>

                </thead>
              </table>
              <br>
              <h4>Lista de Produtos</h4>
              <hr>
              <table class="table" style="font-size:10pt">
                <thead>
                  <tr>
                    <th scope="col">#Código</th>
                    <th scope="col">Imagem</th>
                    <th scope="col">Vendas</th>
                    <th scope="col">Qtd.</th>
                    <th scope="col">Total vendido</th>
                    <th scope="col">Ganho</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($sales as $sale) { ?>

                    <?php $color = ((int)$sale->vendas < 1) ? '#ffffe6' : 'white';  ?>
                  <tr style="background-color:<?php echo $color; ?>">
                    <th scope="row"><?php echo $sale->new_brazil_code; ?></th>
                    <td><img width="50px" height="50px" height="5px" src="data:image/jpeg;base64,<?php echo $sale->image; ?>"/></td>
                    <td><?php echo $sale->vendas; ?></td>
                    <td><?php echo $sale->qtd; ?></td>
                    <td><?php echo $sale->total_price; ?></td>
                    <td><?php echo $sale->total_ganho; ?></td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>



@endsection
