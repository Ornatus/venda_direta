<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script type="text/javascript" src="{{ URL::asset('js/jquery-2.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery-1.11.3.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.maskMoney.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap-select.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('selectpicker/dist/js/i18n/defaults-pt_BR.min.js') }}"></script>


<link rel="stylesheet" type="text/css" href="{{ URL::asset('selectpicker/dist/css/bootstrap-select.min.css') }}">

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
          <script src="https://cdn.rawgit.com/plentz/jquery-maskmoney/master/dist/jquery.maskMoney.min.js"></script>

<table width="100%" cellspacing="5px" border=0>
  <tr>
    <td style="width:20%; padding-left:10px; padding-right:10px">
      <label for="usr">Codigo Brasil:</label>
      <input style="height:30px" type="text" class="form-control"  id="filter_brazil_code_product" value="<?php echo $filter_brazil_code; ?>" />
    </td>
		<td style="width:20%; padding-left:10px; padding-right:10px">
      <label for="usr">Novo Codigo Brasil:</label>
      <input style="height:30px" type="text" class="form-control"  id="filter_new_brazil_code_product" value="<?php echo $filter_new_brazil_code; ?>" />
    </td>
     <td style="width:20%; padding-left:10px; padding-right:10px">
    	<label for="usr">Categoria:</label>
        <select class="selectpicker form-control" multiple data-selected-text-format="count>2" name="filter_category_id" id="filter_category_id">
						<?php foreach($category_list as $category) { ?>
	              <?php $selected_category = in_array($category->category, $filter_category_id) ? 'selected' : '' ?>
	              <option <?php echo $selected_category ?>  value="<?php echo $category->category; ?>"><?php echo $category->category; ?></option>
	          <?php } ?>
        </select>
    </td>
    <td style="vertical-align:bottom; text-align:center">
      <input id="btn-search-product" type="button" class="form-control btn btn-info" style="width:130px;height:35px !important" value="Pesquisar" />
      <input id="new_product" type="button" class="form-control btn btn-primary" data-toggle="modal" data-target=".ModalPreview" data-kit-product="<?php echo $kit_id; ?>" style="width:130px;height:35px !important" value="+ Produto" />
    </td>
  </tr>
</table>
<br>
<!-- div id="search-message" style="text-align:center"><b><?php echo $search_message; ?></b></div-->
<div class="pagination"><?php echo !empty($paginatorprevious) ? $paginatorprevious :'';
                              echo !empty($paginatorpip) ? $paginatorpip :'';
															echo !empty($paginatorperpage) ? $paginatorperpage :'';
															echo !empty($paginatornext) ? $paginatornext :'';
															echo !empty($showing) ? $showing :'';
												 ?></div>

<table id="tblProduct" class="list tablesorter">
  <thead>
    <tr>
      <th class="left table-grid-header">Imagem</th>
       <th class="left table-grid-header">Codigo Brasil</th>
       <th class="left table-grid-header">Codigo Brasil Novo</th>
       <th class="left table-grid-header">Preço</th>
       <th class="left table-grid-header">Preço Revendedora</th>
       <th class="left table-grid-header">Ganho</th>
       <th class="center table-grid-header">Descrição</th>
       <th class="center table-grid-header">Categoria</th>
       <th class="center table-grid-header">Cor</th>
       <th class="center table-grid-header">Ações</th>
    </tr>
  </thead>
  <tbody>

    <?php
    foreach($products as $product)
    {

      ?>
      <tr>
				<td class="center table-grid-data">
            	<img class="img" src="data:image/png;base64,<?php echo $product->image; ?>" alt="<?php echo $product->brazil_code; ?>" height="85" width="98" style="padding: 1px; border: 1px solid #DDDDDD;" />
        </td>
        <td class="left"><?php echo $product->brazil_code ; ?></td>
        <td class="left"><?php echo $product->new_brazil_code ; ?></td>
        <td class="left"><?php echo number_format($product->price, 2, '.', '') ; ?></td>
        <td class="left"><?php echo number_format($product->representative_price, 2, '.', ''); ?></td>
        <td class="left"><?php echo number_format(($product->representative_price - $product->price), 2, '.', ''); ?></td>
        <td class="left"><?php echo $product->description ; ?></td>
        <td class="left"><?php echo $product->category ;?></td>
				<td class="left"><?php echo $product->color ;?></td>
        <td class="center">

          <input
            type="button"
            data-product="<?php echo $product->product_id; ?>"
            data-kit="<?php echo $product->kit_id; ?>"
            class="btn <?php echo !empty($product->kit_id) ? 'btn-warning' : 'btn-primary' ; ?> btn-select-product-search"
            id="edit-kits-<?php echo $product->product_id ?>"
            value="<?php echo !empty($product->kit_id) ? '-' : '+' ; ?>">

        </td>
      </tr>
    <?php
    }
    ?>
  </tbody>
</table>
<div class="pagination"><?php echo !empty($paginatorprevious) ? $paginatorprevious :'';
                              echo !empty($paginatorpip) ? $paginatorpip :'';
															echo !empty($paginatornext) ? $paginatornext :'';
															echo !empty($showing) ? $showing :'';
												 ?></div>

<script>
$(document).ready(function(){
	//pagination
  $('.nextpage').click(function(){

		var page = $(this).data('next');

    getProductsPage(page);
	});

	$('.previouspage').click(function(){

		var page = $(this).data('previous');

    getProductsPage(page);
	});

  $('#btn-search-product').click(function(){
    var kit_id = $("#kit_id").val();
    var filter_brazil_code = $("#filter_brazil_code_product").val();
    var filter_new_brazil_code = $("#filter_new_brazil_code_product").val();

    var categories_id = '';
    $('#filter_category_id :selected').each(function(i, selected)
    {
      if(categories_id == '')
      {
        categories_id = this.value;
      }
      else
      {
        categories_id = categories_id +','+this.value;
      }
    });

    getProducts(kit_id, filter_brazil_code, filter_new_brazil_code, categories_id,'search',1);
  });


  function getProductsPage(page)
  {

    var kit_id = $("#kit_id").val();
    var filter_brazil_code = $("#filter_brazil_code_product").val();
    var filter_new_brazil_code = $("#filter_new_brazil_code_product").val();

    var categories_id = '';
    $('#filter_category_id :selected').each(function(i, selected)
    {
      if(categories_id == '')
      {
        categories_id = this.value;
      }
      else
      {
        categories_id = categories_id +','+this.value;
      }
    });

    getProducts(kit_id, filter_brazil_code, filter_new_brazil_code, categories_id, page);

  }


  $('.btn-select-product').click(function(){
    var button_id = $(this).attr('id');
    var product_id = $(this).data('product');
    var product_kit = $(this).data('kit');
    var kit_id = $("#kit_id").val();

    if(product_kit!="" && $(this).val()=="-")
    {
      removeProductKit(kit_id,product_id,button_id);
    }
    else
    {
      saveProductKit(kit_id,product_id,button_id);
    }
  });

  $('.btn-select-product-search').click(function(){
    var button_id = $(this).attr('id');
    var product_id = $(this).data('product');
    var product_kit = $(this).data('kit');
    var kit_id = $("#kit_id").val();

    if(product_kit!="" && $(this).val()=="-")
    {
      removeProductKit(kit_id,product_id,button_id);
      $("#tr-product-"+kit_id+'-'+product_id).hide();
    }
    else
    {
      saveProductKit(kit_id,product_id,button_id);
    }
  });

 //era para estar na pagina kit list mas nao funcionava la
	function saveProductKit(kit_id, product_id, button_id) {
			var url = "{{ $url_save_kit_product }}";
			var save = false;

			$("#"+button_id).attr("disabled",true);

			$.ajax({
					url: url,
					type: 'POST',
					data: {"kit_id": kit_id,	"product_id": product_id, "_token":"{{ csrf_token() }}"},
					datatype : "application/json",
					success: function(dataReturn) {
							if (dataReturn > 0) {
                  $("#grid-kit").html('');
									$("#" + button_id).val('-');
									$("#" + button_id).data("kit", kit_id);
									$("#" + button_id).removeClass("btn-primary");
									$("#" + button_id).addClass("btn-warning");
									getKitProducts(kit_id);
									//updateModelQuantity(week_id)
							} else {
							 if(dataReturn == 'no-edit')
							 {
								 swal.fire("Erro", "Não foi possivel inserir o produto.", "error");
											return false;
							 }
									swal.fire("Erro", "Não foi possivel inserir o produto.(1))", "error");
									return false;
							}
					},
					error: function(xhr, status, error) {
							swal.fire("Erro", "Não foi possivel inserir o produto.(2))", "error");
							return false;
					}
			}).done(function() {
			 $("#"+button_id).attr("disabled",false);
			});
	}


	function removeProductKit(kit_id, product_id, button_id) {
	    var url = "{{ $url_remove_kit_product }}";
	    var save = false;

	    $("#"+button_id).attr("disabled",true);

	    $.ajax({
				url: url,
				type: 'POST',
				data: {"kit_id": kit_id,	"product_id": product_id, "_token":"{{ csrf_token() }}"},
				datatype : "application/json",
	        success: function(dataReturn) {
	            if (dataReturn > 0) {
	                $("#" + button_id).val('+');
	                $("#" + button_id).removeClass("btn-warning");
	                $("#" + button_id).addClass("btn-primary");
	                //getWeekProducts(week_id);
	                //updateModelQuantity(week_id)
	            } else {

	            	if(dataReturn == 'no-edit')
	            	{
	            		swal.fire("Erro", "Não foi possivel remover o produto.", "error");
	                    return false;
	            	}

	                swal.fire("Erro", "Não foi possivel remover o produto.(3))", "error");
	                return false;
	            }
	        },
	        error: function(xhr, status, error) {
	            swal.fire("Erro", "Não foi possivel remover o produto.(4))", "error");
	            return false;
	        }

	    }).done(function() {
	    	$("#"+button_id).attr("disabled",false);
	    });
	}


	function getProducts(kit_id, filter_brazil_code, filter_new_brazil_code, filter_category_id, page) {
		 var url = "{{ $url_products }}";

			//loading
			$("#modal-body").html("<div class='text-center'><i class='fa fa-spinner fa-spin pull-center' style='font-size:20px'></i></div>");

			$.ajax({
				url: url,
				type: 'GET',
				data: {"kit_id": kit_id, "filter_brazil_code": filter_brazil_code, "filter_new_brazil_code": filter_new_brazil_code, "filter_category_id": filter_category_id, "page": page, "_token":"{{ csrf_token() }}" },
				datatype : "application/json",
				success: function(data) {
						$("#modal-body").html(data.html);
				},
				cache: false,
			});


	}
	/*function updateModelQuantity(week_id) {
	    url = "('catalog/product_reorder/getWeekById/', 'token=' . $this->session->data['token'], '') ?>";
	    url = url.replace(/&amp;/g, '&');

	    $.ajax({
	        url: url,
	        type: 'GET',
	        data: {
	            week_id: week_id
	        },
	        success: function(data) {
	            var week = JSON.parse(data);
	            $("#model-qtd-"+week_id).html(week.count_model);
	            $("#product-qtd-"+week_id).html(week.count_product);
	        }
	    });
	}*/


	function getKitProducts(kit_id, filter_brazil_code, filter_new_brazil_code) {
			var url = "{{ $url_kit_products }}";

			$.ajax({
				url: url,
				type: 'GET',
				data: {"kit_id": kit_id, "filter_brazil_code": filter_brazil_code, "filter_new_brazil_code": filter_new_brazil_code, "_token":"{{ csrf_token() }}" },
				datatype : "application/json",
				success: function(data) {
					$("#"+kit_id).html('');
						 $("#"+kit_id).html(data.html);
				},
				cache: false,
			});

	}

$('.selectpicker').selectpicker();
});

//add product
$(document).ready(function() {
  $('#new_product').click(function() {
      $("#span-kit").text(' No Kit ' + $(this).data('kit-product'));
      $("#kit_product_id").val($(this).data('kit-product'));

  });
$('#save_product').click(function() {
          var kit_id = $("#kit_product_id").val();
          var brazil_code = $("#brazil_code").val();
          var new_brazil_code = $("#new_brazil_code").val();
          var quantity = $("#quantity").val();
          var price = $("#price").val();
          var representative_price = $("#representative_price").val();
          var description = $("#description").val();
          var category = $("#category").val();
          var color = $("#color").val();

           var url = "{{ $url_create_product }}";

           if(brazil_code == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe o Codigo Brasil !",
                 type: "warning",
               });
             return false;
           }
           if(new_brazil_code == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe o Codigo Brasil Novo !",
                 type: "warning",
               });
             return false;
           }
           if(quantity == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe a Quantidade !",
                 type: "warning",
               });
             return false;
           }
           if(price == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe o Preço !",
                 type: "warning",
               });
             return false;
           }
           if(representative_price == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe o Preço Revendedora !",
                 type: "warning",
               });
             return false;
           }
           if(description == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe a Descrição !",
                 type: "warning",
               });
             return false;
           }
           if(category == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe a Categoria !",
                 type: "warning",
               });
             return false;
           }
           if(color == '')
           {
             swal.fire({
                 title: "Atenção",
                 text: "Informe a Cor !",
                 type: "warning",
               });
             return false;
           }

           swal.fire({
            title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
            text: "Aguarde..",
            showCancelButton: false,
            showConfirmButton: false,
            closeOnConfirm: false,
            showLoaderOnConfirm: false,
          });

              $.ajax({
                  url: url,
                  type: 'POST',
                  data: {"kit_id": kit_id,
                         "brazil_code": brazil_code,
                         "new_brazil_code": new_brazil_code,
                         "quantity": quantity,
                         "price": price,
                         "representative_price": representative_price,
                         "description": description,
                         "category": category,
                         "color": color,
                         "_token":"{{ csrf_token() }}"},
                  datatype : "application/json",
                  success: function(data) {
                    if (data > 0)
                    {
                    $("#kit_product_id").val(kit_id);
                    $("#brazil_code").val('');
                    $("#new_brazil_code").val('');
                    $("#quantity").val('');
                    $("#price").val('');
                    $("#representative_price").val('');
                    $("#description").val('');
                    $("#category").val('');
                    $("#color").val('');

                    $("#btn-search-product").click();
                    $("#close-modal").click();
                     swal.close();

                    } else
                    {
                      swal.fire({
                          title: "Erro",
                          text: "Erro ao cadastrar o produto.",
                          type: "warning",
                        });
                      return false;
                    }

                  },
                  error: function(xhr, status, error) {

                  }
              }).done(function() {



              });


          });

     $("#price").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
     $("#representative_price").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

      });
</script>
