@extends('layouts.app')

<style>
.navbar_bottom {
  overflow: hidden;
  background-color:  #FFFFFF;
  position: fixed;
  bottom: 0;
  border:1px solid #d3e0e9;
  width: 100%;
  padding:10px !important;
}

.navbar_bottom a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.navbar_bottom a:hover {
  background: #ddd;
  color: black;
}
</style>
@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <form id="shopping_list" name="shopping_list" method="GET">
          <div class="panel-body">
            <a style="float:right !important" href="{{ URL::to('/representative/send-kit') }}" class="btn button" ><i class="fas fa-reply"></i> Voltar</a>
            <h4><i class="fa fa-bars"></i> <b><?php echo $kit[0]->name ?></b></h4>

            <div class="col-md-12">
              <table class="table" style="font-size:12pt !important">
                <thead>
                  <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Celular</th>
                    <th scope="col">Enviado</th>
                    <th scope="col">Data do envio</th>
                    <th scope="col">Receber Kits</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($representatives as $rep){ ?>
                    <tr>
                      <td><?php echo $rep->name ?></td>
                      <td><?php echo $rep->cellphone ?></td>
                      <td><?php echo ($rep->received=='S') ? 'SIM' : 'NÃO' ?></td>
                      <td><?php echo $rep->date_received ?></td>
                      <td style="width:20% !important;">
                        <?php if( $rep->enabled_send == 1){ ?>
                          <a type="button" title="Habilitado para receber kit" data-status="0" data-kit="<?php echo $kit[0]->id; ?>" data-rep="<?php echo $rep->representative_id; ?>" class="btn btn-success">
                            Habilitado
                          </a>
                        <?php }else{ ?>
                          <a type="button" title="Clique para habilitar o envio do Kit" data-status="1" data-kit="<?php echo $kit[0]->id; ?>" data-rep="<?php echo $rep->representative_id; ?>" class="btn btn-info btn-enable">
                            Desabilitado
                          </a>
                        <?php } ?>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>


<script>


$(document).ready(function(){

  $('.btn-enable').click(function(){

    $btn = $(this);
    $kit = $(this).data('kit');
    $rep = $(this).data('rep');
    $status = $(this).data('status');

    //Não pode desabilitar
    if( $status == 0)
    {
      return false;
    }

    $btn.attr('disabled',true);

    $url = "{{ URL::to('/representative/send-kit/enable-kit/') }}"+"/"+$rep+"/"+$kit+"/"+$status;

    $.ajax({
      url: $url,
      type: 'GET',
      data: {"_token":"{{ csrf_token() }}"},
      datatype : "application/json",
      success: function(dataReturn)
      {
        $convert = JSON.stringify(dataReturn);
        $json = JSON.parse($convert);

        if($json.response == true)
        {
            if($status == 1)
            {
              swal('Habilitado com sucesso!');
              $btn.attr('disabled',false);
              $btn.removeClass('btn-info');
              $btn.removeClass('btn-enable');
              $btn.addClass('btn-success');
              $btn.html('Habilitado');
              $btn.attr('disabled',false);
              console.log($btn.data('status','0'));
            }
            else
            {
              swal('Desabilitado!');
              $btn.attr('disabled',false);
              $btn.addClass('btn-info');
              $btn.removeClass('btn-success');
              $btn.html('Desabilitado');
              $btn.attr('disabled',false);
              $btn.attr('data-status','1');
              console.log($btn.data('status','1'));
            }
        }

      },
      error: function(xhr, status, error) {
        swal('Erro ao habilitar o representante.'+error.text);
        $btn.attr('disabled',false);
        return false;
      }
    }).done(function() {
      $btn.attr('disabled',false);
    });

  });

});



</script>


@endsection
