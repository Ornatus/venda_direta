<table id="tblWeekProduct" class="list tablesorter">
  <thead>
    <tr>
      <th class="left table-grid-header">Imagem</th>
       <th class="left table-grid-header">Codigo Brasil</th>
       <th class="left table-grid-header">Codigo Brasil Novo</th>
       <th class="left table-grid-header">Preço</th>
       <th class="left table-grid-header">Preço Revendedora</th>
       <th class="left table-grid-header">Ganho</th>
       <th class="center table-grid-header">Descrição</th>
       <th class="center table-grid-header">Categoria</th>
       <th class="center table-grid-header">Cor</th>
       <th class="center table-grid-header">Ações</th>
    </tr>
  </thead>
  <tbody>

    <?php
    foreach($products as $product)
    {

      ?>
      <tr id="tr-product-<?php echo $product->kit_id; ?>-<?php echo $product->product_id; ?>">
        <td class="center table-grid-data">
            	<img class="img" src="data:image/png;base64,<?php echo $product->image; ?>" alt="<?php echo $product->brazil_code; ?>" height="42" width="42" style="padding: 1px; border: 1px solid #DDDDDD;" />
        </td>
        <td class="left"><?php echo $product->brazil_code ; ?></td>
        <td class="left"><?php echo $product->new_brazil_code ; ?></td>
        <td class="left"><?php echo number_format($product->price, 2, '.', '') ; ?></td>
        <td class="left"><?php echo number_format($product->representative_price, 2, '.', ''); ?></td>
        <td class="left"><?php echo number_format(($product->representative_price - $product->price), 2, '.', ''); ?></td>
        <td class="left"><?php echo $product->description ; ?></td>
        <td class="left"><?php echo $product->category ;?></td>
				<td class="left"><?php echo $product->color ;?></td>
        <td class="center table-grid-data">
          <?php
               $disabled ='';
            if($product->edit != 0){
              $disabled ='disabled';
               } ?>
	          <input
	            type="button"
	            data-product="<?php echo $product->product_id; ?>"
	            data-kit="<?php echo $product->kit_id; ?>"
	            class="btn <?php echo !empty($product->kit_id) ? 'btn-warning' : 'btn-primary' ; ?>  btn-select-product"
	            id="edit-kite-<?php echo $product->kit_id ?>-<?php echo $product->product_id ?>"
	            value="<?php echo !empty($product->kit_id) ? '-' : '+' ; ?>" <?php echo $disabled; ?>>

        </td>
      </tr>
    <?php
    }
    ?>
  </tbody>
</table>

<script>

$(document).ready(function(){

  $('.btn-select-product').click(function(){
    var button_id = $(this).attr('id');
    var product_id = $(this).data('product');
    var product_kit = $(this).data('kit');
    var kit_id = $(this).data('kit');

    if(product_kit!="" && $(this).val()=="-")
    {
      removeProductKit(kit_id,product_id,button_id);
      $("#tr-product-"+kit_id+'-'+product_id).hide();
    }
    else
    {
      saveProductKit(kit_id,product_id,button_id);
    }
  });

  function saveProductKit(kit_id, product_id, button_id) {
     var url = "{{ $url_save_kit_product }}";
     var save = false;

     $("#"+button_id).attr("disabled",true);

     $.ajax({
         url: url,
         type: 'POST',
         data: {"kit_id": kit_id,	"product_id": product_id, "_token":"{{ csrf_token() }}"},
         datatype : "application/json",
         success: function(dataReturn) {
             if (dataReturn > 0) {
                 $("#" + button_id).val('-');
                 $("#" + button_id).data("kit", kit_id);
                 $("#" + button_id).removeClass("btn-primary");
                 $("#" + button_id).addClass("btn-warning");
                 getKitProducts(kit_id);
                 //updateModelQuantity(week_id)
             } else {
              if(dataReturn == 'no-edit')
              {
                Swal.fireSwal.fire("Erro", "Não foi possivel inserir o produto.", "error");
                     return false;
              }
                 Swal.fire("Erro", "Não foi possivel inserir o produto.(1))", "error");
                 return false;
             }
         },
         error: function(xhr, status, error) {
             Swal.fire("Erro", "Não foi possivel inserir o produto.(2))", "error");
             return false;
         }
     }).done(function() {
      $("#"+button_id).attr("disabled",false);
     });
 }


 function removeProductKit(kit_id, product_id, button_id) {
     var url = "{{ $url_remove_kit_product }}";
     var save = false;

     $("#"+button_id).attr("disabled",true);

     $.ajax({
       url: url,
       type: 'POST',
       data: {"kit_id": kit_id,	"product_id": product_id, "_token":"{{ csrf_token() }}"},
       datatype : "application/json",
         success: function(dataReturn) {
             if (dataReturn > 0) {
                 $("#" + button_id).val('+');
                 $("#" + button_id).removeClass("btn-warning");
                 $("#" + button_id).addClass("btn-primary");
                 //getKitProducts(kit_id);
                 //updateModelQuantity(week_id)
             } else {

               if(dataReturn == 'no-edit')
               {
                 Swal.fire("Erro", "Não foi possivel remover o produto.", "error");
                     return false;
               }

                 Swal.fire("Erro", "Não foi possivel remover o produto.(3))", "error");
                 return false;
             }
         },
         error: function(xhr, status, error) {
             Swal.fire("Erro", "Não foi possivel remover o produto.(4))", "error");
             return false;
         }

     }).done(function() {
       $("#"+button_id).attr("disabled",false);
     });
 }



});



</script>

<style>
.table-grid-header
{
    background-color:#EFEFEF !important;
}

.table-grid-data
{
    background-color:white !important;
}
</style>
