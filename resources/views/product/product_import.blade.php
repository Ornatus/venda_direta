@extends('layouts.app')

<style>
.navbar_bottom {
  overflow: hidden;
  background-color:  #FFFFFF;
  position: fixed;
  bottom: 0;
  border:1px solid #d3e0e9;
  width: 100%;
  padding:10px !important;
}

.navbar_bottom a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.navbar_bottom a:hover {
  background: #ddd;
  color: black;
}
</style>
@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
          <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<div class="container">
  <div class="row">
    <div class="col-md-18">
      <div class="panel panel-default">
        <form id="shopping_list" name="shopping_list" method="GET">
          <div class="panel-body">
            <h4><i class="fa fa-bars"></i><b> Importar Produtos</b></h4>
               <br>
            <div class="col-md-12">
              <form action="{{ action('Product\ProductController@putProduct')  }}" method="post" enctype="multipart/form-data" id="form">
                 <input type="file" name="file" class="btn btn-default">
                 <br>
                 <input type="button" class="btn btn-primary" name="send-file" id="send-file" value="Importar arquivo">
              </form>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
       var files;
       var url = "{{ $url_put_products }}";

       $('input[type=file]').on('change', prepareUpload);
       function prepareUpload(event){
         files = event.target.files;
       };
       $(':button').click(function(){

   			if($('input[type=file]').val().length == 0)
   			{
   				swal.fire({
   				  title: "Nenhum arquivo foi selecionado",
   				  text: "Selecione o arquivo para importação.",
   				  showConfirmButton: true,
   				  allowOutsideClick: false,
   				  type:"warning"
   				});

   				return false;
   			}


	  	  swal.fire({
	  		  title: '<i class="fa fa-spinner fa-spin fa-5x fa-fw" style="font-size:50px"></i>',
	  		  text: "Importando os dados",
	  		  showCancelButton: false,
	  		  showConfirmButton: false,
	  		  closeOnConfirm: false,
	  		  showLoaderOnConfirm: false,
	  		});

           var formData = new FormData();
          formData.append('files', $('input[type=file]')[0].files[0]);

           $.ajax({
               method : 'POST',
               headers: {'X-CSRF-TOKEN': '{{ csrf_token()}}'},
               url: url,
               data: formData ,
               cache: false,
               processData: false,
               contentType : false,
               success:function(data)
   						{
                        if (data){
                           window.location.href=data;

					       // swal.fire("Arquivo importado com sucesso","","success");
                        }
                        else {
                        	swal.fire("Erro Arquivo nao importado","","error");
                        }

             		$('input[type=file]').val('');
               },
               error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
             }
           });
       });
   });

</script>


</div>

@endsection
