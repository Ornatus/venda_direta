@extends('layouts.app')

<style>
.navbar_bottom {
  overflow: hidden;
  background-color:  #FFFFFF;
  position: fixed;
  bottom: 0;
  border:1px solid #d3e0e9;
  width: 100%;
  padding:10px !important;
}

.navbar_bottom a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.navbar_bottom a:hover {
  background: #ddd;
  color: black;
}
</style>
@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <form id="shopping_list" name="shopping_list" method="GET">
          <div class="panel-body">
            <h4><i class="fa fa-bars"></i><b> Lista de Representantes </b></h4>
            <div class="col-md-12">
              <a style="float:right !important" href="{{ URL::to('/representative') }}" class="btn button" ><i class="fas fa-plus"></i> Novo Cadastro</a>
              <h4>Geral</h4>
              <table class="table" style="font-size:12pt !important">
                <thead>
                  <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Status</th>
                    <th scope="col">Empresa</th>
                    <th scope="col">Celular</th>
                    <th scope="col">Nível</th>
                    <th scope="col">Ações</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($representativeDatas as $item){ ?>

                    <tr>
                      <th scope="row"><?php echo $item->name?></th>
                      <td><?php echo $item->status == '1' ? 'habilitado' : 'Desabilitado' ?></td>
                      <th scope="row"><?php echo $item->customer ?></th>
                      <th scope="row"><?php echo $item->cellphone ?></th>
                      <th scope="row"><?php echo $item->level ?></th>
                      <td> <a id='btn-edit' href="{{ URL::to('/representative/representativeEdit/') }}<?php echo '/'.$item->id ?>" class='btn button btn-edit'><i class="fas fa-edit"></i> Editar</a></td>

                    </tr>

                  <?php } ?>
                </tbody>

              </table>

            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>



@endsection
