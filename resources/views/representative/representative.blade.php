@extends('layouts.app')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
				<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
				<script type="text/javascript" src="{{ URL::asset('js/jquery-2.1.1.min.js') }}"></script>
				<script type="text/javascript" src="{{ URL::asset('js/jquery.maskedinput.js') }}"></script>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastro de Representante</div>

              <div class="panel-body">
                  <div class="col-md-12">
                    <a style="float:right !important" href="{{ URL::to('/representative/representativeList') }}" class="btn button" ><i class="fas fa-reply"></i> Voltar</a>
                  </div>
                  <br />
                    <form class="form-horizontal" method="POST" action="{{ action('Representative\RepresentativeController@verify')  }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Nome</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-md-4 control-label">Status</label>

                            <div class="col-md-6">

                                <select name="status" id="status" class="form-control" required>
                                  <option value="1" >habilitado</option>
                                  <option value="0" >Desabilitado</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="level" class="col-md-4 control-label">Nível</label>

                            <div class="col-md-6">

                                <select name="level" id="level" class="form-control" required>
                                  <option value="1" >Nível 1</option>
                                  <option value="2" >Nível 2</option>
                                  <option value="3" >Nível 3</option>
                                  <option value="4" >Nível 4</option>
                                  <option value="5" >Nível 5</option>
                                  <option value="6" >Nível 6</option>
                                  <option value="7" >Nível 7</option>
                                  <option value="8" >Nível 8</option>
                                  <option value="9" >Nível 9</option>
                                  <option value="10" >Nível 10</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-md-4 control-label">Cliente</label>

                            <div class="col-md-6">

                                <select name="customer_id" id="customer_id" class="form-control" required>
                                  <option value="0"> </option>

                               <?php foreach ( $customer_list as $customer ){ ?>

                                   <option value="<?php echo $customer->id; ?>" ><?php echo $customer->name; ?></option>

                                 <?php } ?>
                                </select>
                            </div>
                        </div>

												<div class="form-group">
													<label for="status" class="col-md-4 control-label">Celular</label>

													<div class="col-md-6">

									        <input type="txtcellphone" id="cellphone" name="cellphone" class="form-control"  value="" />
												</div>
												</div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Cadastrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$('#cellphone').focusout(function(){

		 var phone = cellphone.value;
		 console.log(phone);
			 var phone, element;
			 element = $(this);
			 element.unmask();
			 phone = element.val().replace(/\D/g, '');
			 if(phone.length > 11) {
					 element.mask("(99) 99999-9999");
			 } else {
					 element.mask("(99) 99999-9999");
			 }
	 }).trigger('focusout');


</script>
@endsection
