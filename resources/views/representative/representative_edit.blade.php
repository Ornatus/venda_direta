@extends('layouts.app')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Empresa</div>

                <div class="panel-body">
                  <div class="col-md-12">
                    <a style="float:right !important" href="{{ URL::to('/representative/representativeList') }}" class="btn button" ><i class="fas fa-reply"></i> Voltar</a>
                  </div>
                  <br />
                    <form class="form-horizontal" method="POST" action="{{ action('Representative\RepresentativeController@representativeUpdate')  }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Nome</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="<?php echo $representativeData['name'] ?>" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-md-4 control-label">Status</label>

                            <div class="col-md-6">

                                <select name="status" id="status" class="form-control" required>
                                  <?php
                                      $select = '';
                                  if ($representativeData['status'] == '1') {
                                       $select = 'selected';

                                  }
                                      $select2 = '';
                                  if ($representativeData['status'] == '0') {
                                       $select2 = 'selected';

                                  } ?>

                                  <option value="1" <?php echo $select ?> >habilitado</option>
                                  <option value="0" <?php echo $select2 ?> >Desabilitado</option>
                                </select>
                                <input id="id" type="hidden" class="form-control" name="id" value="<?php echo $representativeData['id'] ?>" >
                            </div>
													</div>
														<div class="form-group">
		                            <label for="level" class="col-md-4 control-label">Nível</label>
														<div class="col-md-6">

                                <select name="level" id="level" class="form-control" required>
                                  <?php
                                      $select = '';
                                  if ($representativeData['status'] == '1') {
                                       $select = 'selected';

                                  }
                                      $select2 = '';
                                  if ($representativeData['status'] == '2') {
                                       $select2 = 'selected';

                                  }
																			$select3 = '';
																	if ($representativeData['status'] == '3') {
																			 $select3 = 'selected';

																	}
																			$select4 = '';
																	if ($representativeData['status'] == '4') {
																			 $select4 = 'selected';

																	}
																			$select5 = '';
																	if ($representativeData['status'] == '5') {
																			 $select5 = 'selected';

																	}
																			$select6 = '';
																	if ($representativeData['status'] == '6') {
																			 $select6 = 'selected';

																	}
																			$select7 = '';
																	if ($representativeData['status'] == '7') {
																			 $select7 = 'selected';

																	}
																			$select8 = '';
																	if ($representativeData['status'] == '8') {
																			 $select8 = 'selected';

																	}
																			$select9 = '';
																	if ($representativeData['status'] == '9') {
																			 $select9 = 'selected';

																	}
																			$select10 = '';
																	 if ($representativeData['status'] == '10') {
																				$select10 = 'selected';

																	 }

																	?>
																	<option value="1" <?php echo $select ?>>Nível 1</option>
																	<option value="2" <?php echo $select2 ?>>Nível 2</option>
																	<option value="3" <?php echo $select3 ?>>Nível 3</option>
																	<option value="4" <?php echo $select4 ?>>Nível 4</option>
																	<option value="5" <?php echo $select5 ?>>Nível 5</option>
																	<option value="6" <?php echo $select6 ?>>Nível 6</option>
																	<option value="7" <?php echo $select7 ?>>Nível 7</option>
																	<option value="8" <?php echo $select8 ?>>Nível 8</option>
																	<option value="9"<?php echo $select9 ?>>Nível 9</option>
                                  <option value="10"<?php echo $select10 ?>>Nível 10</option>
                                </select>
															</div>
                        </div>
												<div class="form-group">
														<label for="status" class="col-md-4 control-label">Cliente</label>

														<div class="col-md-6">

																<select name="customer_id" id="customer_id" class="form-control" required>
																	<option value="0"> </option>

															    <?php foreach ( $customer_list as $customer ){ ?>
																		<?php if ($customer->id == $representativeData['customer_id']) { ?>

																			<option value="<?php echo $representativeData['customer_id']; ?>" selected="selected"><?php echo $customer->name; ?></option>

																		<?php } else { ?>

																			<option value="<?php echo $customer->id; ?>" ><?php echo $customer->name; ?></option>

																		<?php } ?>
																	<?php } ?>
																</select>
														</div>
												</div>

												<div class="form-group">
													<label for="status" class="col-md-4 control-label">Celular</label>

													<div class="col-md-6">

													<input type="txtcellphone" id="cellphone" name="cellphone" class="form-control"  value="<?php echo $representativeData['cellphone'] ?>" />
												</div>
												</div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Editar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
