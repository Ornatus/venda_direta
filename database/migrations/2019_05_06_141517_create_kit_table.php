<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('kit', function (Blueprint $table) {
          $table->increments('id');
          $table->datetime('date_start');
          $table->datetime('date_end');
          $table->integer('user_id');
          $table->timestamps('date_added');
          $table->integer('active');
          $table->datetime('date_confirmed');
          $table->string('confirmed');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('kit');
    }
}
