<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUserKitIdInTableSyncSaleProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::table('sync_sale_product', function (Blueprint $table) {

         $table->integer('user_id')->nullable();
         $table->integer('kit_id')->nullable();

       });
     }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::table('sync_sale_product', function($table) {
         $table->dropColumn('user_id');
         $table->dropColumn('kit_id');
       });
     }
}
