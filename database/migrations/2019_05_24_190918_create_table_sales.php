<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('sale', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('user_id')->nullable();
           $table->integer('kit_id')->nullable();
           $table->integer('venda_id')->nullable();
           $table->string('data_criacao')->nullable();
           $table->string('data_finalizacao')->nullable();
           $table->string('finalizada')->nullable();
           $table->timestamp('data_importacao')->useCurrent();
           $table->integer('ativo')->default(1);
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('sale');
     }
}
