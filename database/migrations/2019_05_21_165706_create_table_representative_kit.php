<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRepresentativeKit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('representative_kit', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('user_id');
           $table->integer('representative_id')->nullable();
           $table->integer('kit_id')->nullable();
           $table->string('enabled_send', 50)->default(0);
           $table->timestamp('date_enabled')->useCurrent();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('representative_kit');
     }
}
