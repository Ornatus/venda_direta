<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSyncSaleProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('sync_sale_product', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('venda_produto_id')->nullable();
           $table->string('data_criacao')->nullable();
           $table->integer('venda_id')->nullable();
           $table->integer('product_id')->nullable();
           $table->integer('qtd')->nullable();
           $table->timestamp('data_importacao')->useCurrent();
           $table->integer('ativo')->default(1);
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('sync_sale_product');
     }
}
