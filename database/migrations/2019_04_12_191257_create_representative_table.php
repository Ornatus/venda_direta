<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepresentativeTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('representative', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('status');
        $table->integer('customer_id');
        $table->integer('cellphone');
        $table->timestamps('date_added');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('representative');
  }
}
