<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEnabledReceiveingRepresentativeKit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::table('representative_kit', function (Blueprint $table) {

         $table->string('sync')->default('N');
         $table->timestamp('date_sync')->nullable();
       });
     }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::table('representative_kit', function($table) {
         $table->dropColumn('sync');
         $table->dropColumn('date_sync');
       });
     }
}
