<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnReceivedInRepresentativeKitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::table('representative_kit', function (Blueprint $table) {

         $table->string('received')->default('N');
         $table->timestamp('date_received')->nullable();
       });
     }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::table('representative_kit', function($table) {
         $table->dropColumn('received');
         $table->dropColumn('date_received');
       });
     }
}
