<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKitProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('kit_product', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('kit_id');
          $table->integer('product_id');
          $table->integer('quantity');
          $table->timestamps('date_added');
          $table->integer('active');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kit_product');
    }
}
