<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('sync_cliente', function (Blueprint $table) {
           $table->increments('id');
           $table->string('user_id')->nullable();
           $table->string('cliente_id')->nullable();
           $table->string('nome')->nullable();
           $table->string('telefone')->nullable();
           $table->timestamp('data_importacao')->useCurrent();
           $table->integer('ativo')->default(1);
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('sync_cliente');
     }
}
