<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduct extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('product', function (Blueprint $table) {
        $table->increments('id');
        $table->string('brazil_code', 50);
        $table->string('new_brazil_code', 50);
        $table->text('image');
        $table->decimal('price', 8, 2);
        $table->decimal('representative_price', 8, 2);
        $table->text('description');
        $table->string('category', 100);
        $table->string('color', 50);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('product');
  }
}
