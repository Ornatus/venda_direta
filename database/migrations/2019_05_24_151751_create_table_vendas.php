<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVendas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('vendas', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('user_id')->nullable();
           $table->integer('kit_id')->nullable();
           $table->integer('venda_id')->nullable();
           $table->integer('product_id')->nullable();
           $table->string('dt_criacao_venda')->nullable();
           $table->string('dt_finalizacao_venda')->nullable();
           $table->integer('quantidade')->nullable();
           $table->timestamp('data_sync')->useCurrent();
           $table->integer('ativo')->default(0);
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('vendas');
     }
}
