<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnClienteValorAcerto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::table('sale', function (Blueprint $table) {

         $table->string('cliente_id')->nullable();
         $table->string('valor_pago')->nullable();

       });
     }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::table('sale', function($table) {
         $table->dropColumn('cliente_id');
         $table->dropColumn('valor_pago');
       });
     }
}
