<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsPayInRepresentativeKit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::table('representative_kit', function (Blueprint $table) {

         $table->string('enabled_pay')->default('N');
         $table->timestamp('date_pay')->nullable();
         $table->string('pay')->default('N');
       });
     }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::table('representative_kit', function($table) {
         $table->dropColumn('enabled_pay');
         $table->dropColumn('date_pay');
         $table->dropColumn('pay');
       });
     }
}
