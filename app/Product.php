<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
  static function getProducts($data)
  {
    $sql = "SELECT
                (SELECT kit_id
                    FROM kit_product
                    WHERE product_id = p.id
                    AND active = 1
                    AND kit_id = '".$data['kit_id']."'
                    LIMIT 1
                  ) as kit_id,
                  p.id as product_id,
                  p.brazil_code,
                  p.new_brazil_code,
                  p.image,
                  p.price,
                  p.representative_price,
                  p.description,
                  p.category,
                  p.color
               from product p where 1 ";

    if(!empty($data['filter_brazil_code']) )
    {
      $sql .= " AND p.brazil_code like '%".$data['filter_brazil_code']."%' ";
    }

    //aquii filtro novo codigo Brasil
    if(!empty($data['filter_new_brazil_code']) )
    {
      $sql .= " AND p.new_brazil_code like '%".$data['filter_new_brazil_code']."%' ";
    }

    if (isset($data['filter_category_id']) && is_array($data['filter_category_id']) && count($data['filter_category_id']) > 0)
    {
    	$categories = "'".implode("','", $data['filter_category_id'])."'";

      if(!empty($categories))
      {
        $sql.= " AND p.category in (  " .$categories. " )";
      }

    }

    $sql .=" ORDER BY category ASC LIMIT ".$data['offset'].", ".$data['limit']." ";

    return (array)DB::select($sql);
  }

  static function getTotalProducts($data)
  {
    $sql = "SELECT p.id from product p where 1";

    if(!empty($data['filter_brazil_code']) )
    {
      $sql .= " AND p.brazil_code like '%".$data['filter_brazil_code']."%' ";
    }

    //aquii filtro novo codigo Brasil
    if(!empty($data['filter_new_brazil_code']) )
    {
      $sql .= " AND p.new_brazil_code like '%".$data['filter_new_brazil_code']."%' ";
    }

    if (isset($data['filter_category_id']) && is_array($data['filter_category_id']) && count($data['filter_category_id']) > 0)
    {
      $categories = "'".implode("','", $data['filter_category_id'])."'";

      if(!empty($categories))
      {
        $sql.= " AND p.category in
             (  " .$categories. " )";
      }

    }

    return (array)DB::select($sql);
  }

  static function getCategoriesFilter()
  {
    $sql = "SELECT DISTINCT(category) from product ";


    return (array)DB::select($sql);
  }

  static function putImportProduct($data)
  {
    $sql = "INSERT INTO import_product
		                                SET
                                    size= '". $data['size']."',
						                        directory= '". $data['directory']."',
						                        date_added= now()";

    $imported = (array)DB::select($sql);

    return DB::getPdo()->lastInsertId();
  }

  static function insertFile($data)
  {

    $sql = "INSERT INTO product
    SET
    brazil_code = '".$data['brazil_code']."',
    new_brazil_code = '".$data['new_brazil_code']."',
    image = '".$data['image']."',
    price = '".$data['price']."',
    representative_price = '".$data['representative_price']."',
    description = '".$data['description']."',
    category = '".$data['category']."',
    color = '".$data['color']."',
    import_product_id = '".$data['import_product_id']."',
    hash = '".$data['hash']."'";

    $kit_product =  (array)DB::select($sql);

    return DB::getPdo()->lastInsertId();

  }

  static function getProductsByHash($data)
  {
    $sql = "SELECT
                  p.id as product_id,
                  p.brazil_code,
                  p.new_brazil_code,
                  p.image,
                  p.price,
                  p.representative_price,
                  p.description,
                  p.category,
                  p.color
               from product p WHERE hash ='".$data."'";

    return (array)DB::select($sql);
  }
}
