<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Kit extends Model
{
  protected $fillable = ['date_start', 'date_end', 'user_id', 'date_added', 'active', 'date_confirmed', 'confirmed', 'name', 'parent_id', 'level'];
  protected $table = 'kit';

  static function getKits()
  {
    $sql = "SELECT k.*,
    CASE
    WHEN current_date between date_start and date_end
    OR date_end < current_date then 0
    ELSE 1
    END AS edit
    FROM kit k
    WHERE k.active = 1
    AND k.date_start > current_date";

    return (array)DB::select($sql);
  }

  static function getkitsHistory()
  {
    $sql = "SELECT k.*,
    CASE
    WHEN current_date between k.date_start and k.date_end
    OR k.date_end < current_date then 0
    ELSE 1
    END AS edit
    FROM kit k
    WHERE k.active = 1
    AND k.date_start <= current_date
    ORDER BY k.date_start DESC ";

    return (array)DB::select($sql);
  }

  static function getNextKit()
  {
    $sql = "SELECT
    DATE_ADD(max(DATE(date_end)),INTERVAL 1 DAY) as date_start,
    DATE_ADD(max(DATE(date_end)),INTERVAL 1 MONTH) as date_end
    FROM kit LIMIT 1";

    return (array)DB::select($sql);
  }

  static function getKitProducts($data)
  {

    $sql = "SELECT
    (SELECT kit_id
      FROM kit_product
      WHERE product_id = p.id
      AND active = 1
      AND kit_id = '".$data['kit_id']."'
      LIMIT 1
    ) as kit_id,
    (SELECT CASE
      WHEN (current_date BETWEEN date_start and date_end) OR confirmed ='S'
      then 1
      ELSE 0
      END AS edit
      FROM kit
      WHERE active = 1
      AND id = '".$data['kit_id']."'
      LIMIT 1
    ) as edit,
    p.id as product_id,
    p.brazil_code,
    p.new_brazil_code,
    p.image,
    p.price,
    p.representative_price,
    p.description,
    p.category,
    p.color
    FROM product p WHERE p.id IN (SELECT product_id
      FROM kit_product
      WHERE active = 1
      AND kit_id = '".$data['kit_id']."'
    ) ";

    if(!empty($data['filter_brazil_code']) )
    {
      $sql .= " AND p.brazil_code like '%".$data['filter_brazil_code']."%' ";
    }

    //aquii filtro novo codigo Brasil
    if(!empty($data['filter_new_brazil_code']) )
    {
      $sql .= " AND p.new_brazil_code like '%".$data['filter_new_brazil_code']."%' ";
    }

    return (array)DB::select($sql);

  }

  static function isCurrentKit($data)
  {

    $sql = "SELECT  k.*,
    (
      SELECT count(1)
      FROM kit_product
      WHERE kit_id = k.id
      AND active = 1
    ) as count_product,
    (
      SELECT count( distinct p.new_brazil_code)
      FROM kit_product kp
      JOIN product p on p.id = kp.product_id
      WHERE kit_id = k.id
      AND active = 1
    ) as count_model,
    CASE
    WHEN current_date between date_start and date_end
    OR date_end < current_date then 0
    ELSE 1
    END AS edit
    FROM kit k
    WHERE k.active = 1
    AND k.id = '".$data."'";

    $kit = (array)DB::select($sql);

    if(count($kit)>0 )
    {
      if( $kit[0]->edit == 0 )
      {
        return true;
      }

    }

    return false;
  }

  static function saveKitProduct($data)
  {

    $sql = "INSERT INTO kit_product
    SET
    kit_id = '".$data['kit_id']."',
    product_id   = '".$data['product_id']."',
    quantity     = 15,
    active     = 1";

    $kit_product =  (array)DB::select($sql);

    return DB::getPdo()->lastInsertId();

  }

  static function removeKitProduct($data)
  {

    $sql = "UPDATE kit_product
    SET active = 0
    WHERE
    kit_id = '".$data['kit_id']."' AND
    product_id   = '".$data['product_id']."' AND
    active     = 1";

    return  DB::update($sql);

  }

  static function getConfirmedKits()
  {
    $sql = "SELECT id, level, date(date_start) as date_start, date(date_end) as date_end, name  FROM kit WHERE confirmed = 'S'";

    return (array)DB::select($sql);
  }

  static function getKitById($id)
  {
    $sql = "SELECT *  FROM kit WHERE id = '".$id."'";

    return (array)DB::select($sql);
  }

  static function putKitProductParents($data)
  {

    $sql = "INSERT INTO kit_product
    SET
    kit_id = '".$data['kit_id']."',
    product_id   = '".$data['product_id']."',
    quantity     = '".$data['quantity']."',
    active     = 1";

    $kit_product =  (array)DB::select($sql);

    return DB::getPdo()->lastInsertId();

  }

  static function kitConfirm($data)
  {

    $sql = "UPDATE kit
    SET
        confirmed ='S'
    WHERE
    id = '".$data['kit_id']."' AND confirmed ='N'";

    $kit_product =  (array)DB::select($sql);

    return 1;

  }

}
