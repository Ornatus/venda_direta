<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Customer extends Model
{
  protected $fillable = ['name','status'];
  protected $table = 'customer';

}
