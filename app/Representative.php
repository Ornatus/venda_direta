<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Representative extends Model
{
  protected $fillable = ['name','status', 'customer_id', 'cellphone','level'];
  protected $table = 'representative';

  static function getCustomers()
  {
    $sql = "SELECT * FROM customer WHERE status ='1'";

    return (array)DB::select($sql);
  }

  static function getRepresentatives()
  {
    $sql = "SELECT r.id,
              	   r.name,
                     r.status,
                     c.name as customer,
                     r.cellphone,
                     r.level
                     FROM representative r
                      JOIN customer c ON r.customer_id = c.id ";

    return (array)DB::select($sql);
  }

  static function getRepresentativeByCellphone($cellphone)
  {

    $sql = "SELECT r.id,
              	   r.name,
                     r.status,
                     c.name as customer,
                     c.id as customer_id,
                     r.cellphone,
                     r.level
                     FROM representative r
                      JOIN customer c ON r.customer_id = c.id
                      WHERE r.cellphone = '". $cellphone ."' ";

    return (array)DB::select($sql);
  }

  static function getRepresentativeListByLevel($level, $kit)
  {
        $sql = "SELECT
                  r.id as representative_id,
                  rk.kit_id,
                  r.name,
                  r.level,
                  r.cellphone,
                  rk.id as rep_kit_id,
                  rk.enabled_send,
                  rk.received,
                  rk.date_received,
                  rk.enabled_pay,
                  rk.sync,
                  rk.date_sync
                FROM representative r
                LEFT JOIN representative_kit rk ON rk.representative_id = r.id AND rk.kit_id = '".$kit."'
                WHERE r.level = '".$level."'";

       return (array)DB::select($sql);
  }

  static function getAvailableKit($id)
  {
        $sql = "SELECT
                	k.name as kit_name,
                  k.id as kit_id,
                    DATE_FORMAT(date(k.date_start),'%d/%m/%Y') as date_start,
                    DATE_FORMAT(date(k.date_end),'%d/%m/%Y') as date_end
                FROM representative r
                	JOIN representative_kit rk ON rk.representative_id = r.id
                	JOIN kit k on k.id = rk.kit_id
                	WHERE r.id = '".$id."'
                	AND current_date >=date(k.date_start)
                  AND current_date <= date(k.date_end)
                  AND rk.enabled_send = 1
                  AND rk.received = 'N';";

       return (array)DB::select($sql);
  }

  static function getProductsByKit($kit)
  {
        $sql = "SELECT * FROM kit_product kp
                  JOIN product p ON p.id = kp.product_id
                  AND kp.active = 1
                  AND kp.kit_id = '".$kit."'
                  ORDER BY new_brazil_code;";

       return (array)DB::select($sql);
  }

  static function receivedProducts($kit,$representative_id)
  {
        $sql = "UPDATE representative_kit
                  SET received='S', date_received = current_timestamp
                  WHERE kit_id = '".$kit."'
                  AND representative_id = '".$representative_id."'";

       return (array)DB::select($sql);
  }

  static function getAvailableKitToPay($id)
  {
        $sql = "SELECT
                  k.name as kit_name,
                  k.id as kit_id,
                    DATE_FORMAT(date(k.date_start),'%d/%m/%Y') as date_start,
                    DATE_FORMAT(date(k.date_end),'%d/%m/%Y') as date_end
                FROM representative r
                  JOIN representative_kit rk ON rk.representative_id = r.id
                  JOIN kit k on k.id = rk.kit_id
                  WHERE r.id = '".$id."'
                  AND current_date >=date(k.date_start)
                  AND current_date <= date(k.date_end)
                  AND rk.enabled_send = 1
                  AND rk.received = 'S'
                  AND rk.enabled_pay = 'S'
                  AND rk.sync = 'N';";

       return (array)DB::select($sql);
  }

  static function cleantSales($kit, $representative_id)
  {
    $update = "UPDATE sale SET ativo = 0 WHERE user_id = '".$venda->user_id."' AND kit_id = '".$kit."';";
    DB::select($update);
  }

  static function cleanSaleProduct($kit,$representative_id)
  {
    $update = "UPDATE sale_product SET ativo = 0 WHERE user_id = '".$representative_id."' AND kit_id = '".$kit."';";
    DB::select($update);
  }

  static function insertSales($kit, $venda)
  {
    $sql = "INSERT into sale (user_id, kit_id, venda_id, data_criacao, data_finalizacao, finalizada, cliente_id, valor_pago, ativo)
    VALUES ('".$venda->user_id."', '".$kit."', '".$venda->id."', '".$venda->data_criacao."', '".$venda->data_finalizacao."', '".$venda->finalizada."','".$venda->cliente_id."','".$venda->valor_pago."','1')";

    return (array)DB::insert($sql);
  }

  static function insertSaleProduct($kit,$user_id, $product)
  {
    $sql = "INSERT into sale_product (kit_id, user_id, venda_produto_id, data_criacao, venda_id, product_id, qtd, ativo)
    VALUES ('".$kit."','".$user_id."','".$product->id."', '".$product->data_criacao."', '".$product->venda_id."', '".$product->product_id."', '".$product->qtd."','1')";

    return (array)DB::insert($sql);
  }

  static function setKitReceived($kit, $representative_id)
  {
    $update = "UPDATE representative_kit SET sync = 'S', date_sync = current_timestamp WHERE kit_id = '".$kit."' AND representative_id = '".$representative_id."'";

    DB::select($update);
  }

  static function getSalesByKitAndRepresentative($kit_id, $representative_id)
  {
        $sql = "SELECT
                p.id,
                p.new_brazil_code,
                p.image,
                p.price,
                sum(sp.qtd*(p.representative_price)) as total_price,
                sum(sp.qtd*(p.representative_price-p.price)) as total_ganho,
                (p.representative_price-p.price) as ganho,
                count(distinct sp.venda_id) as vendas,
                sum(sp.qtd) as qtd
              FROM kit_product kp
              JOIN product p on p.id = kp.product_id
              LEFT JOIN sale s on s.kit_id = kp.kit_id AND s.ativo = '1'
              LEFT JOIN sale_product sp on sp.venda_id = s.venda_id and p.id = sp.product_id AND s.kit_id = sp.kit_id AND s.user_id = sp.user_id AND sp.ativo = '1'
              WHERE kp.active = 1
              AND kp.kit_id = '".$kit_id."'
              AND s.user_id = '".$representative_id."'
              GROUP BY p.id, p.new_brazil_code,  p.image,p.price
              ORDER BY count(distinct sp.venda_id) desc, p.new_brazil_code asc;";

       return (array)DB::select($sql);
  }

  static function getTotalSalesByKitAndRepresentative($kit_id, $representative_id)
  {
        $sql = "SELECT
                  SUM(sp.qtd * (p.representative_price)) AS total_price,
                  SUM(sp.qtd * (p.representative_price - p.price)) AS total_ganho,
                  COUNT(DISTINCT sp.venda_id) AS vendas,
                  COUNT(DISTINCT sp.product_id) AS produtos_vendidos,
                  COUNT(DISTINCT kp.product_id) AS total_produtos
              FROM
                  kit_product kp
                      JOIN
                  product p ON p.id = kp.product_id
                      LEFT JOIN
                  sale s ON s.kit_id = kp.kit_id AND s.ativo = '1'
                      LEFT JOIN
                  sale_product sp on sp.venda_id = s.venda_id and p.id = sp.product_id AND s.kit_id = sp.kit_id AND s.user_id = sp.user_id AND sp.ativo = '1'
              WHERE
                  kp.active = 1 AND kp.kit_id = '".$kit_id."'
                      AND s.user_id = '".$representative_id."';";

       return (array)DB::select($sql);
  }

  static function cleanSyncSales($kit, $representative_id)
  {
    $update = "UPDATE sync_sale SET ativo = 0 WHERE user_id = '".$representative_id."' AND kit_id = '".$kit."';";
    DB::select($update);

  }

  static function cleanSyncSaleProduct($kit,$representative_id)
  {
    $update = "UPDATE sync_sale_product SET ativo = 0 WHERE user_id = '".$representative_id."' AND kit_id = '".$kit."';";
    DB::select($update);
  }

  static function insertSyncSales($kit, $venda)
  {
    $sql = "INSERT into sync_sale (user_id, kit_id, venda_id, data_criacao, data_finalizacao, finalizada, cliente_id, valor_pago, ativo)
    VALUES ('".$venda->user_id."', '".$kit."', '".$venda->id."', '".$venda->data_criacao."', '".$venda->data_finalizacao."', '".$venda->finalizada."', '".$venda->cliente_id."', '".$venda->valor_pago."','1')";

    return (array)DB::insert($sql);
  }

  static function insertSyncSaleProduct($kit,$user_id, $product)
  {

    $sql = "INSERT into sync_sale_product (kit_id, user_id, venda_produto_id, data_criacao, venda_id, product_id, qtd, ativo)
    VALUES ('".$kit."','".$user_id."','".$product->id."', '".$product->data_criacao."', '".$product->venda_id."', '".$product->product_id."', '".$product->qtd."','1')";

    return (array)DB::insert($sql);
  }

  static function lastSync($kit_id, $user_id)
  {
    $sql = "select time_to_sec(TIMEDIFF(current_timestamp,max(data_importacao))) as minutos_ultima_importacao, max(data_importacao) as data_hora from sync_sale where user_id = '".$kit_id."' AND kit_id = '".$user_id."';";
    $query = DB::select($sql);

    return $query;

  }

  static function getPartialSalesByKitAndRepresentative($kit_id, $representative_id)
  {
        $sql = "SELECT
                p.id,
                p.new_brazil_code,
                p.image,
                p.price,
                sum(sp.qtd)*(p.representative_price) as total_price,
                sum(sp.qtd)*(p.representative_price-p.price) as total_ganho,
                (p.representative_price-p.price) as ganho,
                count(distinct sp.venda_id) as vendas,
                sum(sp.qtd) as qtd
              FROM kit_product kp
              JOIN product p on p.id = kp.product_id
              LEFT JOIN sync_sale s on s.kit_id = kp.kit_id AND s.ativo = '1'
              LEFT JOIN sync_sale_product sp on sp.venda_id = s.venda_id and p.id = sp.product_id AND s.kit_id = sp.kit_id AND s.user_id = sp.user_id AND sp.ativo = '1'
              WHERE kp.active = 1
              AND kp.kit_id = '".$kit_id."'
              AND s.user_id = '".$representative_id."'
              GROUP BY p.id, p.new_brazil_code,  p.image,p.price
              ORDER BY count(distinct sp.venda_id) desc, p.new_brazil_code asc;";

       return (array)DB::select($sql);
  }

  static function getTotalPartialSalesByKitAndRepresentative($kit_id, $representative_id)
  {
        $sql = "SELECT
                  SUM(sp.qtd * (p.representative_price)) AS total_price,
                  SUM(sp.qtd * (p.representative_price - p.price)) AS total_ganho,
                  COUNT(DISTINCT sp.venda_id) AS vendas,
                  COUNT( sp.product_id) AS produtos_vendidos,
                  COUNT(DISTINCT kp.product_id) AS total_produtos
              FROM
                  kit_product kp
                      JOIN
                  product p ON p.id = kp.product_id
                      LEFT JOIN
                  sync_sale s ON s.kit_id = kp.kit_id AND s.ativo = '1'
                  LEFT JOIN sync_sale_product sp on sp.venda_id = s.venda_id and p.id = sp.product_id AND s.kit_id = sp.kit_id AND s.user_id = sp.user_id AND sp.ativo = '1'
                      AND p.id = sp.product_id
                      AND sp.ativo = '1'
              WHERE
                  kp.active = 1 AND kp.kit_id = '".$kit_id."'
                      AND s.user_id = '".$representative_id."';";

       return (array)DB::select($sql);
  }


  static function insertSyncCliente($kit,$user_id, $cliente)
  {

    $sql = "INSERT into sync_cliente (user_id, cliente_id, nome, telefone)
            VALUES ('".$user_id."', '".$cliente->id."', '".$cliente->nome."', '".$cliente->telefone."')";

    return (array)DB::insert($sql);
  }

  static function insertCliente($kit,$user_id, $cliente)
  {

    $sql = "INSERT into cliente (user_id, cliente_id, nome, telefone)
            VALUES ('".$user_id."', '".$cliente->id."', '".$cliente->nome."', '".$cliente->telefone."')";

    return (array)DB::insert($sql);
  }

  static function cleanSyncCliente($kit,$representative_id)
  {
    $update = "UPDATE sync_cliente SET ativo = 0 WHERE user_id = '".$representative_id."';";
    DB::select($update);
  }

  static function cleanCliente($kit,$representative_id)
  {
    $update = "UPDATE cliente SET ativo = 0 WHERE user_id = '".$representative_id."';";
    DB::select($update);
  }



}
