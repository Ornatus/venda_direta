<?php

namespace App\Http\Controllers\Kit;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use App\Kit;
use App\Product;
use Illuminate\Pagination\Paginator;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class KitController extends Controller
{
  /**
   * Create a new controller instance.
   *
  * @return void
  */
 public function __construct()
 {
     $this->middleware('auth');

 }

 /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
     $view = array();

     $view['kit'] = Kit::getKits();

     return view('kit/kit',$view);
 }

 /**
  * Get a validator for an incoming registration request.
  *
 *@param  array  $data
* @return \Illuminate\Contracts\Validation\Validator
*/
 public function kitList()
 {
     $view['kitDatas'] = Kit::getkits();
     $view['url_new_kit'] = URL::to('/kit/kitCreate');
     $view['url_create_kit'] = URL::to('/kit/kitNew');
     $view['url_grid_kit'] = URL::to('/kit/kitGrid');
     $view['url_history_kit'] = URL::to('/kit/kitHistoryGrid');
     $view['url_products'] = URL::to('/kit/getProducts');
     $view['url_kit_products'] = URL::to('/kit/getKitProducts');
     $view['url_save_kit_product'] = URL::to('/kit/saveKitProduct');

     $view['html'] = view('kit/kit_grid', $view)->render();

     return view('kit/kit_list', $view);
 }

 public function kitConfigById($id)
 {

     $view['kit_id'] = '';
     if(isset( $id ))
     {
       $view['kit_id'] = $id;
     }

     $kitData = Kit::where('id',$id)->first();

     $view['kitData'] = $kitData->toArray();

     //$view['products'] = Product::getProducts($view);
     $view['url_new_kit'] = URL::to('/kit/kitCreate');
     $view['url_create_kit'] = URL::to('/kit/kitNew');
     $view['url_grid_kit'] = URL::to('/kit/kitGrid');
     $view['url_history_kit'] = URL::to('/kit/kitHistoryGrid');
     $view['url_products'] = URL::to('/kit/getProducts');
     $view['url_kit_products'] = URL::to('/kit/getKitProducts');
     $view['url_save_kit_product'] = URL::to('/kit/saveKitProduct');
     $view['url_remove_kit_product'] = URL::to('/kit/removeKitProduct');
     $view['url_confirm_kit'] = URL::to('/kit/kitConfirm');
     $view['url_create_product'] = URL::to('/product/productCreate');

     $view['products'] = Kit::getKitProducts($view);

     $view['html'] = view('kit/product_kit_show', $view)->render();

     return view('kit/kit_config', $view);
 }

 public function kitGrid()
 {

   $view['kitDatas'] = Kit::getkits();

   $view['html'] = view('kit/kit_grid', $view)->render();

   return Response::json($view);
 }

 public function kitHistoryGrid()
 {

   $view['kitDatas'] = Kit::getkitsHistory();

   $view['html'] = view('kit/kit_grid', $view)->render();

   return Response::json($view);
 }


 public function kitCreate()
 {

   $nextKit = Kit::getNextKit();
   $view['date_start'] = $nextKit[0]->date_start;

   if(empty($view['date_start'] ))
   {
     $view['date_start'] = date('Y-m-d', strtotime("first day of +1 month"));
   }

   $view['date_end'] = $nextKit[0]->date_end;

   if(empty( $view['date_end'] ))
   {
     $view['date_end'] = date('Y-m-d', strtotime($view['date_start']. ' + 1 month -1 day'));
   }

   return Response::json(json_encode($view));
 }

 public function kitNew(Request $request)
 {
   $data['date_start'] = $request->date_start;
   $data['date_end'] = $request->date_end;
   $data['user_id'] = '1';
   $data['created_at'] = '2019-04-01 09:37:00';
   $data['updated_at'] = '2019-04-01 09:37:00';
   $data['active'] = '1';
   $data['date_confirmed'] = '2019-04-01 09:37:00';
   $data['confirmed'] = 'N';
   $data['user_confirmed'] = '0';

   $kit = Kit::create($data);
     return Response::json('1');
 }

 public function getProducts(Request $request)
 {
   $view['url_save_kit_product'] = URL::to('/kit/saveKitProduct');
   $view['url_remove_kit_product'] = URL::to('/kit/removeKitProduct');
   $view['url_kit_products'] = URL::to('/kit/getKitProducts');
   $view['url_products'] = URL::to('/kit/getProducts');
   $view['url_create_product'] = URL::to('/product/productCreate');
   $view['filter_brazil_code'] = '';
   $view['filter_new_brazil_code'] = '';
   $view['kit_id'] = '';
   $view['filter_category_id'] = array();
   $view['search_message'] = 'last_models_not_available';
   $view['products_without_week'] = true;
   $view['url'] = '';
   $view['page'] = 1;
   $view['offset'] = 0;
   $view['limit'] = 20;

   if(isset( $request->filter_brazil_code ))
   {
     $view['filter_brazil_code'] = $request->filter_brazil_code;
   }

   if(isset( $request->filter_new_brazil_code ))
   {
     $view['filter_new_brazil_code'] = $request->filter_new_brazil_code;
   }

   if(isset( $request->kit_id ))
   {
     $view['kit_id'] = $request->kit_id;
   }


   $filter_category_id = '';
   if (isset($request->filter_category_id)) {
    $filter_category_id= $request->filter_category_id;
    $filter_category_id = explode(',', $filter_category_id);
    $view['filter_category_id'] = $filter_category_id;
   }


   //Se apertou o botão de buscar
   //if(isset( $this->request->get['type'] ) && $this->request->get['type'] =='search' )
   //{
    $view['search_message'] = 'search_result';
    $view['products_without_week'] = false;
   //}

  // criar array de categorias
   $categoryList = Product::getCategoriesFilter();

   $view['category_list'] = $categoryList;

   if(isset($request->page) && $request->page > 1)
   {
     $view['page'] = $request->page;
     $view['offset'] = ( $view['page'] -1 ) * $view['limit'];
   }

    $view['products'] = Product::getProducts($view);


    $view['totalProducts'] = Product::getTotalProducts($view);

    // paginação.

    $previous = $view['page'] -1;
    $next = $view['page'] +1;
    $tp = ceil(count($view['totalProducts']) / $view['limit']) ;
    $firstnumber  = $view['offset'] < 20 ? $view['limit'] : $view['offset']+1;
    $pg = ($firstnumber == 20) ?  1 : $firstnumber;
    $pgtwo = $view['offset'] >= 20 ?  $view['offset']+20 : 20;

    if ($view['page'] > 1) {
      $view['paginatorprevious'] = "<a type='button' class='btn btn-default previouspage' data-previous='$previous'><- Anterior</a> ";
    }



    $view['paginatorpip'] = "|";

    $view['showing'] = "   Exibindo ".$pg." até ".$pgtwo." de ".count($view['totalProducts'])." (".$tp." páginas)";

    if ($view['page']<$tp)
    {
      $view['paginatornext'] = " <a type='button' class='btn btn-default nextpage' data-next='$next'>Próxima -></a>  ";
    }

    $view['html'] = view('kit/product_kit_modal', $view)->render();

   return Response::json($view);
 }

 public function getKitProducts(Request $request)
 {

   $view['url_save_kit_product'] = URL::to('/kit/saveKitProduct');
   $view['url_remove_kit_product'] = URL::to('/kit/removeKitProduct');
   $view['url_kit_products'] = URL::to('/kit/getKitProducts');
   $view['filter_brazil_code'] = '';
   $view['filter_new_brazil_code'] = '';
   $view['kit_id'] = '';

   if(isset( $request->filter_brazil_code ))
   {
     $view['filter_brazil_code'] = $request->filter_brazil_code;
   }

   if(isset( $request->filter_new_brazil_code ))
   {
     $view['filter_new_brazil_code'] = $request->filter_new_brazil_code;
   }

   if(isset( $request->kit_id ))
   {
     $view['kit_id'] = $request->kit_id;
   }

   $view['products'] = Kit::getKitProducts($view);

   $view['html'] = view('kit/product_kit_show', $view)->render();

   return Response::json($view);
 }

 public function saveKitProduct(Request $request)
 {
   $view['kit_id'] = $request->kit_id;
   $view['product_id'] = $request->product_id;

   $kit = Kit::isCurrentKit($view['kit_id']);

   if($kit)
   {
     //print 'no-edit';
     //return;
     return Response::json('no-edit');
   }

   $save = Kit::saveKitProduct($view);

   return Response::json($save);
 }

 public function removeKitProduct(Request $request)
 {
   $view['kit_id'] = $request->kit_id;
   $view['product_id'] = $request->product_id;

   $kit = Kit::isCurrentKit($view['kit_id']);

   if($kit)
   {
     //print 'no-edit';
     //return;
     return Response::json('no-edit');
   }

   $save = Kit::removeKitProduct($view);

   return Response::json($save);
 }

 public function kitConfig($id)
 {
   $view['products'] = Product::getProductsByHash($id);
   $view['hash'] = $id;
   return view('kit/kit_form', $view);
 }

 public function kitCreateAtomatic(Request $request)
 {

  $data['name'] = $request->name;
  $data['date_start'] = $request->date_start;
  $data['date_end'] = $request->date_end;
  $data['hash'] = $request->hash;
  $data['user_id'] = '1';
  $data['created_at'] = '2019-04-01 09:37:00';
  $data['updated_at'] = '2019-04-01 09:37:00';
  $data['active'] = '1';
  $data['date_confirmed'] = '2019-04-01 09:37:00';
  $data['confirmed'] = 'N';
  $data['user_confirmed'] = '0';
  $data['parent_id'] = '0';
  $data['level'] = '1';

  $kit = Kit::create($data);
  $products_imported = Product::getProductsByHash($data['hash']);

  if (isset($products_imported)) {
      foreach ($products_imported as $product) {

            $data['kit_id'] = $kit->id;
            $data['product_id'] = $product->product_id;

            $save = Kit::saveKitProduct($data);
      }
  }



  $data['kit_id'] = $kit->id;
  for ($i = 2; $i <= 4; $i++) {

      $this->kitCreateAtomaticParents($i, $data);
  }


   return redirect('/kit/kitList');

 }

 public function kitCreateAtomaticParents($i, $data)
 {
   $case = $i;

   switch($case) {
       default:
       case '2':
                 $data['name'] = $data['name'].'.'.$case;
                 $data['date_start'] = $data['date_start'];
                 $data['date_end'] = $data['date_end'];
                 $data['hash'] = $data['hash'];
                 $data['user_id'] = '1';
                 $data['created_at'] = '2019-04-01 09:37:00';
                 $data['updated_at'] = '2019-04-01 09:37:00';
                 $data['active'] = '1';
                 $data['date_confirmed'] = '2019-04-01 09:37:00';
                 $data['confirmed'] = 'N';
                 $data['user_confirmed'] = '0';
                 $data['parent_id'] = $data['kit_id'];
                 $data['level'] = '2';

                 $kit = Kit::create($data);
                 $products_imported = Product::getProductsByHash($data['hash']);

                 if (isset($products_imported)) {
                     foreach ($products_imported as $product) {

                           $data['kit_id'] = $kit->id;
                           $data['product_id'] = $product->product_id;
                           $data['quantity'] = '10';

                           $save = Kit::putKitProductParents($data);
                     }
                 }

         break;
       case '3':
                 $data['name'] = $data['name'].'.'.$case;
                 $data['date_start'] = $data['date_start'];
                 $data['date_end'] = $data['date_end'];
                 $data['hash'] = $data['hash'];
                 $data['user_id'] = '1';
                 $data['created_at'] = '2019-04-01 09:37:00';
                 $data['updated_at'] = '2019-04-01 09:37:00';
                 $data['active'] = '1';
                 $data['date_confirmed'] = '2019-04-01 09:37:00';
                 $data['confirmed'] = 'N';
                 $data['user_confirmed'] = '0';
                 $data['parent_id'] = $data['kit_id'];
                 $data['level'] = '3';

                 $kit = Kit::create($data);
                 $products_imported = Product::getProductsByHash($data['hash']);

                 if (isset($products_imported)) {
                     foreach ($products_imported as $product) {

                           $data['kit_id'] = $kit->id;
                           $data['product_id'] = $product->product_id;
                           $data['quantity'] = '7';

                           $save = Kit::putKitProductParents($data);
                     }
                 }
         break;
       case '4':
                 $data['name'] = $data['name'].'.'.$case;
                 $data['date_start'] = $data['date_start'];
                 $data['date_end'] = $data['date_end'];
                 $data['hash'] = $data['hash'];
                 $data['user_id'] = '1';
                 $data['created_at'] = '2019-04-01 09:37:00';
                 $data['updated_at'] = '2019-04-01 09:37:00';
                 $data['active'] = '1';
                 $data['date_confirmed'] = '2019-04-01 09:37:00';
                 $data['confirmed'] = 'N';
                 $data['user_confirmed'] = '0';
                 $data['parent_id'] = $data['kit_id'];
                 $data['level'] = '4';

                 $kit = Kit::create($data);
                 $products_imported = Product::getProductsByHash($data['hash']);

                 if (isset($products_imported)) {
                     foreach ($products_imported as $product) {

                           $data['kit_id'] = $kit->id;
                           $data['product_id'] = $product->product_id;
                           $data['quantity'] = '5';

                           $save = Kit::putKitProductParents($data);
                     }
                 }
         break;
     }

 }

 public function kitUpdate(Request $request)
 {
   $id=$request->input('kit_id');
   $kit = Kit::findOrFail($request->input('kit_id'));
   $kit->name = $request->input('name');
   $kit->date_start = $request->input('date_start');
   $kit->date_end = $request->input('date_end');

   $kit->save();

   return redirect('/kit/kitConfigById/'.$id);

 }

 public function kitConfirm(Request $request)
 {

   $data['kit_id'] = $request->kit_id;

   $save = Kit::kitConfirm($data);

   return Response::json($save);

 }

 protected function create(array $data)
 {
     return Kit::create([
         'date_start' => $data['date_start'],
         'date_end' => $data['date_end'],
         'user_id' => '1',
         'active' => '1',
     ]);
 }
}
