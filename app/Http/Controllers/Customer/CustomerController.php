<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Kit;
use App\Representative;
use App\RepresentativeKit;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');

  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    return view('customer/customer');
  }

  /**
  * Get a validator for an incoming registration request.
  *
  *@param  array  $data
  * @return \Illuminate\Contracts\Validation\Validator
  */
  public function customerList()
  {
    $customerData = Customer::get();

    $view['customerDatas'] = $customerData->toArray();

    return view('customer/customer_list', $view);
  }

  public function customerEdit($id)
  {

    $customerData = Customer::where('id',$id)->first();

    $view['customerData'] = $customerData->toArray();

    return view('customer/customer_edit',$view);
    //return Response::json('customer/customer_edit', $view);
  }


  public function verify(Request $request)
  {
    $customer = new Customer;

    $customer->name = $request->input('name');
    $customer->status = $request->input('status');

    $customerData = Customer::create($customer->toArray());

    return redirect('/customer/customerList');
  }

  public function customerUpdate(Request $request)
  {
    $customer = Customer::findOrFail($request->input('id'));
    $customer->name = $request->input('name');
    $customer->status = $request->input('status');

    $customer->save();

    return redirect('/customer/customerList');

  }

  protected function create(array $data)
  {
    return Customer::create([
      'name' => $data['name'],
      'status' => $data['status'],
    ]);
  }

  public function payKitRepresentativeList($kit_id)
  {

    $kit = Kit::getKitById($kit_id);

    $representatives = Representative::getRepresentativeListByLevel($kit[0]->level, $kit[0]->id);

    $view['representatives'] = $representatives;
    $view['kit'] = $kit;

    return view('kit/send_kit_representative_list', $view);
  }

}
