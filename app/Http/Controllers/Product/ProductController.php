<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use App\Product;
use App\Kit;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use File;

class ProductController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
      date_default_timezone_set('America/Sao_Paulo');

  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function indexProduct()
  {
      $view = array();
      $view['url_put_products'] = URL::to('/product/putProduct');

      return view('product/product_import',$view);
  }

  /**
   * Get a validator for an incoming registration request.
   *
  *@param  array  $data
 * @return \Illuminate\Contracts\Validation\Validator
 */

 public function putProduct(Request $request)
 {
    if (isset($_FILES['files'])) {
        date_default_timezone_set("Brazil/East"); //Definindo timezone padr�o

        $ext      = strtolower(substr($_FILES['files']['name'], -4));
        $new_name = date("Y.m.d-H.i.s") . $ext;
        $dir      = 'upload/file-import/';

        if(!is_dir( $dir ))
        {
          mkdir($dir,0777,true);
        }

        if (move_uploaded_file($_FILES['files']['tmp_name'], $dir . $new_name))
        {
            $data['directory']=$dir.$new_name;
            $data['size']=$_FILES['files']['size'];

            $imported_id = Product::putImportProduct($data);
            $imported = $this->importFile($dir . $new_name, $_FILES['files'], $imported_id);
        }
          $imported = URL::to('/kit/kitConfig/'.$imported);
          return Response::json($imported);
          //return Redirect::to('/kit/kitConfig');

    }

  }

  public function importFile($file, $original_file, $imported_id)
	{
		$data     = array();
		$lines    = 0;
		$ponteiro = fopen($file, "r");

    $code = strtoupper ('vd'.dechex(time()).$imported_id);

		while (!feof($ponteiro)) {
			$linha = fgets($ponteiro, 4096);

			if (!empty(trim($linha))) {
				$collums = explode("|", $linha);
        $price= str_replace('R$', '',$collums[5]);
        $price= str_replace(',', '.',$price);
        $representative_price= str_replace('R$', '',$collums[6]);
        $representative_price= str_replace(',', '.',$representative_price);

        $price = $representative_price-$price;

        $data['brazil_code']          = $collums[1];
				$data['new_brazil_code']      = $collums[3];
				$data['image']                = $this->getImage($collums[1]);
				$data['price']                = trim($price);
				$data['representative_price'] = trim($representative_price);
				$data['description']          = $collums[4];
				$data['category']             = $collums[4];
				$data['color']                = $collums[7];
        $data['import_product_id']    = $imported_id;
        $data['hash']                 = $code;


				 Product::insertFile($data);

        $lines++;
			}
		}
		fclose($ponteiro);

		return $code;
	}

  public function getImage($data)
	{
    $http = "https://franqueado.morana.com.br/ppm/index.php?route=product/image/image&width=100&height=100&token=684299e25052d48c2dfeeed11d55be06&code=";
    $image = $http.$data;
    $file = file_get_contents($image);

    $base =  base64_encode($file);

    return $base;
  }

  public function productCreate(Request $request)
  {

    $price = str_replace(',', '.',$request->price);
    $representative_price = str_replace(',', '.',$request->representative_price);

    $data['brazil_code']          = $request->brazil_code;
    $data['new_brazil_code']      = $request->new_brazil_code;
    $data['quantity']             = $request->quantity;
    $data['image']                = $this->getImage($request->brazil_code);
    $data['price']                = trim($price);
    $data['representative_price'] = trim($representative_price);
    $data['description']          = $request->description;
    $data['category']             = $request->category;
    $data['color']                = $request->color;
    $data['import_product_id']    = '';
    $data['hash']                 = '';


     $product_id = Product::insertFile($data);

     $data['kit_id'] = $request->kit_id;
     $data['product_id'] = $product_id;
     $data['quantity'] = $data['quantity'];

     $save = Kit::putKitProductParents($data);
     return $save;
  }
}
