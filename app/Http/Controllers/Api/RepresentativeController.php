<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Representative;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class RepresentativeController extends Controller
{
  //

  public function getRepresentativeByCellphone($cellphone)
  {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: PUT, GET, POST");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

    $representative = Representative::getRepresentativeByCellphone($cellphone);

    return Response::json(json_encode($representative));
  }

  public function getAvailableKit($cellphone)
  {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: PUT, GET, POST");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

    $kit = Representative::getAvailableKit($cellphone);

    return Response::json(json_encode($kit));
  }

  public function getProductsByKit($kit)
  {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: PUT, GET, POST");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

    $products = Representative::getProductsByKit($kit);

    return Response::json(json_encode($products));
  }

  public function receivedProducts($kit, $representative_id)
  {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: PUT, GET, POST");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

    $update = Representative::receivedProducts($kit, $representative_id);

    return Response::json(json_encode($update));
  }

  public function getAvailableKitToPay($user_id)
  {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: PUT, GET, POST");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

    $kit = Representative::getAvailableKitToPay($user_id);

    return Response::json(json_encode($kit));
  }

  public function sendSales(Request $request, $kit, $representative_id)
  {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: PUT, GET, POST");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

    $vendas = (array)(json_decode($request['vendas']));
    $produtos = (array)(json_decode($request['produtos']));
    $clientes = (array)(json_decode($request['clientes']));

    foreach($vendas as $venda)
    {
      Representative::insertSales($kit, $venda);
    }

    foreach($produtos as $produto)
    {
      Representative::insertSaleProduct($kit,$representative_id,$produto);
    }

    Representative::cleanCliente($kit, $representative_id);

    foreach($clientes as $cliente)
    {
      Representative::insertCliente($kit,$representative_id, $cliente);
    }

    Representative::setKitReceived($kit, $representative_id);

    return Response::json(json_encode(true));
  }

  public function sendSaleProduct(Request $request, $kit, $representative_id)
  {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: PUT, GET, POST");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

    $produtos = (array)(json_decode($request['produtos']));

    foreach($produtos as $produto)
    {
      Representative::insertSaleProduct($kit, $produto);
    }

    return Response::json(json_encode(true));
  }

  public function syncSales(Request $request, $kit, $representative_id)
  {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: PUT, GET, POST");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

    $vendas = (array)(json_decode($request['vendas']));
    $produtos = (array)(json_decode($request['produtos']));
    $clientes = (array)(json_decode($request['clientes']));

    $last_sync = Representative::lastSync($kit, $representative_id);

    //  if($last_sync[0]->minutos_ultima_importacao == null || $last_sync[0]->minutos_ultima_importacao > 1800)
    //    {


    Representative::cleanSyncSales($kit, $representative_id);
    Representative::cleanSyncSaleProduct($kit, $representative_id);
    Representative::cleanSyncCliente($kit, $representative_id);

    foreach($vendas as $venda)
    {
      Representative::insertSyncSales($kit, $venda);
    }

    foreach($produtos as $produto)
    {
      Representative::insertSyncSaleProduct($kit,$representative_id, $produto);
    }

    foreach($clientes as $cliente)
    {
      Representative::insertSyncCliente($kit,$representative_id, $cliente);
    }

    //    }

    return Response::json(json_encode(true));
  }


}
