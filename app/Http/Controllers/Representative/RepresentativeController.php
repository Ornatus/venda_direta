<?php

namespace App\Http\Controllers\Representative;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Representative;
use App\Kit;
use App\RepresentativeKit;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;



class RepresentativeController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
      date_default_timezone_set('America/Sao_Paulo');

  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $view = array();

      $view['customer_list'] = Representative::getCustomers();

      return view('representative/representative',$view);
  }

  /**
   * Get a validator for an incoming registration request.
   *
  *@param  array  $data
 * @return \Illuminate\Contracts\Validation\Validator
 */
  public function representativeList()
  {
      $view['representativeDatas'] = Representative::getRepresentatives();

      return view('representative/representative_list', $view);
  }

  public function representativeEdit($id)
  {
     $view = array();

     $representativeData = Representative::where('id',$id)->first();

     $view['customer_list'] = Representative::getCustomers();

     $view['representativeData'] = $representativeData->toArray();

      return view('representative/representative_edit',$view);
      //return Response::json('customer/customer_edit', $view);
  }


  public function verify(Request $request)
  {
      $representative = new Representative;

      $representative->name = $request->input('name');
      $representative->status = $request->input('status');
      $representative->customer_id = $request->input('customer_id');
      $representative->cellphone = $request->input('cellphone');
      $representative->level = $request->input('level');

      $representativeData = Representative::create($representative->toArray());

      return redirect('/representative/representativeList');
  }

  public function representativeUpdate(Request $request)
  {
      $representative = Representative::findOrFail($request->input('id'));
      $representative->name = $request->input('name');
      $representative->status = $request->input('status');
      $representative->customer_id = $request->input('customer_id');
      $representative->cellphone = $request->input('cellphone');
      $representative->level = $request->input('level');

      $representative->save();

      return redirect('/representative/representativeList');

  }

  protected function create(array $data)
  {
      return Representative::create([
          'name' => $data['name'],
          'status' => $data['status'],
          'customer_id' => $data['customer_id'],
          'cellphone' => $data['cellphone'],
          'level' => $data['level'],
      ]);
  }

  public function sendKitRepresentativeList($kit_id)
  {

    $kit = Kit::getKitById($kit_id);

    $representatives = Representative::getRepresentativeListByLevel($kit[0]->level, $kit[0]->id);

    $view['representatives'] = $representatives;
    $view['kit'] = $kit;

    return view('kit/send_kit_representative_list', $view);
  }

  public function sendKit()
  {
    $kits = Kit::getConfirmedKits();

    $view['kits'] = $kits;

    return view('kit/send_kit', $view);
  }

  public function enableKit(Request $request,$representative_id, $kit_id, $status)
  {
    $view['response'] = false;

    $repKit =  RepresentativeKit::getRepresentativeKitByIds($representative_id, $kit_id);

    if(count($repKit)>0)
    {
      $id = $repKit[0]->id;

      $update = RepresentativeKit::where('id',$id)->update(['enabled_send'=>$status, 'date_enabled'=>date('Y-m-d H:i:s') ]);

    //  dd($update);

      $view['response'] = true;

    }
    else
    {
      RepresentativeKit::create(
        ['user_id'=>$request->user()->id,
        'kit_id'=>$kit_id,
        'representative_id'=>$representative_id,
        'enabled_send'=>$status
      ]);

      $view['response'] = true;
    }

    return Response::json($view);
  }

  public function settleKitRepresentativeList($kit_id)
  {

    $kit = Kit::getKitById($kit_id);

    $representatives = Representative::getRepresentativeListByLevel($kit[0]->level, $kit[0]->id);

    $view['representatives'] = $representatives;
    $view['kit'] = $kit;

    return view('kit/settle_kit_representative_list', $view);
  }

  public function enableSettleKit(Request $request,$representative_id, $kit_id, $status)
  {
    $view['response'] = false;

    $repKit =  RepresentativeKit::getRepresentativeKitByIds($representative_id, $kit_id);

    if(count($repKit)>0)
    {
      $id = $repKit[0]->id;

      $update = RepresentativeKit::where('id',$id)->update(['enabled_pay'=>$status, 'date_pay'=>date('Y-m-d H:i:s') ]);

    //  dd($update);

      $view['response'] = true;

    }
    else
    {
      RepresentativeKit::create(
        ['user_id'=>$request->user()->id,
        'kit_id'=>$kit_id,
        'representative_id'=>$representative_id,
        'enabled_pay'=>$status
      ]);

      $view['response'] = true;
    }

    return Response::json($view);
  }

  public function extract($kit_id, $representative_id)
  {
    $view = [];
    $view['sales'] = Representative::getSalesByKitAndRepresentative($kit_id, $representative_id);
    $view['total'] = Representative::getTotalSalesByKitAndRepresentative($kit_id, $representative_id);
    $view['kit']   = Kit::getKitById($kit_id);
    $view['representative'] = Representative::where('id',$representative_id)->first()->toArray();

    return view('kit/extract', $view);
  }


  public function partialExtract($kit_id, $representative_id)
  {
    $view = [];
    $view['sales'] = Representative::getPartialSalesByKitAndRepresentative($kit_id, $representative_id);
    $view['total'] = Representative::getTotalPartialSalesByKitAndRepresentative($kit_id, $representative_id);
    $view['kit']   = Kit::getKitById($kit_id);
    $view['representative'] = Representative::where('id',$representative_id)->first()->toArray();
    $view['lastSync'] = Representative::lastSync($kit_id, $representative_id);

    return view('kit/partial_extract', $view);
  }

}
