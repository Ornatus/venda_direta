<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RepresentativeKit extends Model
{
  protected $fillable = ['user_id','representative_id', 'kit_id', 'enabled_send'];
  protected $table = 'representative_kit';
  public $timestamps = false;

  static function getRepresentativeKitByIds($representative_id, $kit_id)
  {
    $sql = "SELECT * FROM representative_kit
              WHERE kit_id = '".$kit_id."'
                AND representative_id = '".$representative_id."' LIMIT 1";


    return DB::select($sql);
  }



}
