<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//customer routes

Route::get('/customer', 'Customer\CustomerController@index');
Route::post('/customer/verify', 'Customer\CustomerController@verify');
Route::get('/customer/customerList', 'Customer\CustomerController@customerList');
Route::get('/customer/customerEdit/{id}', 'Customer\CustomerController@customerEdit')->name('customer.edit');
Route::post('/customer/customerUpdate', 'Customer\CustomerController@customerUpdate');



Route::get('/representative/send-kit', 'Representative\RepresentativeController@sendKit');
Route::get('/representative/send-kit/representative-list/{kit}', 'Representative\RepresentativeController@sendKitRepresentativeList');
Route::get('/representative/send-kit/enable-kit/{representative_id}/{kit_id}/{status}', 'Representative\RepresentativeController@enableKit');
Route::get('/representative/settle-kit/representative-list/{kit}', 'Representative\RepresentativeController@settleKitRepresentativeList');
Route::get('/representative/settle-kit/enablesettle-kit/{representative_id}/{kit_id}/{status}', 'Representative\RepresentativeController@enableSettleKit');
Route::get('/representative/settle-kit/extract/{kit_id}/{representative_id}', 'Representative\RepresentativeController@extract');
Route::get('/representative/settle-kit/partial-extract/{kit_id}/{representative_id}', 'Representative\RepresentativeController@partialExtract');


//representative  routes

Route::get('/representative', 'Representative\RepresentativeController@index');
Route::post('/representative/verify', 'Representative\RepresentativeController@verify');
Route::get('/representative/representativeList', 'Representative\RepresentativeController@representativeList');
Route::get('/representative/representativeEdit/{id}', 'Representative\RepresentativeController@representativeEdit')->name('representative.edit');
Route::post('/representative/representativeUpdate', 'Representative\RepresentativeController@representativeUpdate');


// api
Route::get('/api/getRepresentativeByCellphone/{cellphone}', 'Api\RepresentativeController@getRepresentativeByCellphone');
Route::get('/api/getAvailableKit/{cellphone}', 'Api\RepresentativeController@getAvailableKit');
Route::get('/api/getProductsByKit/{kit}', 'Api\RepresentativeController@getProductsByKit');
Route::get('/api/receivedProducts/{kit}/{representative_id}', 'Api\RepresentativeController@receivedProducts');
Route::get('/api/sendSales/{kit}/{representative_id}', 'Api\RepresentativeController@sendSales');
Route::get('/api/sendSaleProduct/{kit}/{representative_id}', 'Api\RepresentativeController@sendSaleProduct');
Route::get('/api/getAvailableKitToPay/{representative_id}', 'Api\RepresentativeController@getAvailableKitToPay');
Route::get('/api/syncSales/{kit}/{representative_id}', 'Api\RepresentativeController@syncSales');
//Kit routes

Route::get('/kit', 'Kit\KitController@index');
Route::post('/kit/verify', 'Kit\KitController@verify');
Route::get('/kit/kitList', 'Kit\KitController@kitList');
Route::get('/kit/kitCreate', 'Kit\KitController@kitCreate');
Route::post('/kit/kitNew', 'Kit\KitController@kitNew');
Route::get('/kit/kitGrid', 'Kit\KitController@kitGrid');
Route::get('/kit/kitHistoryGrid', 'Kit\KitController@kitHistoryGrid');
Route::get('/kit/getProducts', 'Kit\KitController@getProducts');
Route::get('/kit/getKitProducts', 'Kit\KitController@getKitProducts');
Route::post('/kit/saveKitProduct', 'Kit\KitController@saveKitProduct');
Route::post('/kit/removeKitProduct', 'Kit\KitController@removeKitProduct');
Route::get('/kit/kitEdit/{id}', 'Kit\KitController@kitEdit')->name('kit.edit');
Route::post('/kit/kitUpdate', 'Kit\KitController@kitUpdate');
Route::get('/kit/kitConfigById/{id}', 'Kit\KitController@kitConfigById');
Route::get('/kit/kitConfig/{id}', 'Kit\KitController@kitConfig');
Route::post('/kit/kitCreateAtomatic', 'Kit\KitController@kitCreateAtomatic');
Route::post('/kit/kitConfirm', 'Kit\KitController@kitConfirm');

//products
Route::get('/product/indexProduct', 'Product\ProductController@indexProduct');
Route::post('/product/putProduct', 'Product\ProductController@putProduct');
Route::post('/product/productCreate', 'Product\ProductController@productCreate');
